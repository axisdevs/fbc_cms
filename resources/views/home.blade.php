@extends('layouts.auth-master')
@section('title')
    {{ __('Create Ticket') }}
@endsection
@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/floating_chat.css') }}">
@endsection
@section('scripts')
    <script src="//cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
    @if(env('CHAT_MODULE') == 'yes')
        <script>
            var old_chat_user = getCookie('chat_user');
            $('#prime').click(function () {
                if (old_chat_user != '') {
                    // has cookie
                    $('.msg_chat').removeClass('d-none');
                    $('.msg_form').removeClass('d-block');
                    $('.msg_chat').addClass('d-block');
                    $('.msg_form').addClass('d-none');

                    getMsg();
                } else {
                    // no cookie
                    $('.msg_chat').removeClass('d-block');
                    $('.msg_form').removeClass('d-none');
                    $('.msg_chat').addClass('d-none');
                    $('.msg_form').addClass('d-block');
                }
                toggleFab();
            });

            //Toggle chat and links
            function toggleFab() {
                $('.chat').toggleClass('is-visible');
                $('.fab').toggleClass('is-visible');
                $('.chat').toggleClass('d-none');
            }

            // Email Form Submit
            $('#chat_frm_submit').on('click', function () {
                var email = $('#chat_email').val();
                $.ajax({
                    type: 'POST',
                    url: '{{ route('chat_form.store') }}',
                    data: {
                        "_token": '{{ csrf_token() }}',
                        email: email,
                    },
                    success: function (data) {
                        if (data != 'false') {
                            setCookie('chat_user', JSON.stringify(data), 30);
                            $('.msg_chat').removeClass('d-none');
                            $('.msg_form').removeClass('d-block');
                            $('.msg_form').addClass('d-none');
                            $('.msg_chat').addClass('d-block');
                        } else if (data == 'false') {
                            $('.e_error').html('Something went wrong.!');
                        }
                    }
                });
            });
            // End Email Form Submit

            $(document).on('keyup', '#chatSend', function (e) {
                var message = $(this).val();
                if (e.keyCode == 13 && message != '') {
                    $(this).val('');

                    $.ajax({
                        type: "post",
                        url: "floating_message",
                        data: {
                            "_token": '{{ csrf_token() }}',
                            message: message,
                        },
                        cache: false,
                        success: function (data) {
                        },
                        error: function (jqXHR, status, err) {
                        },
                        complete: function () {
                            getMsg();
                        }
                    })
                }
            });

            // make a function to scroll down auto
            function scrollToBottomFunc() {
                $('#chat_fullscreen').animate({
                    scrollTop: $('#chat_fullscreen').get(0).scrollHeight
                }, 10);
            }

            // get Message when page is load or when msg successfully send
            function getMsg() {
                $.ajax({
                    type: "get",
                    url: "{{ route('get_message') }}",
                    data: "",
                    cache: false,
                    success: function (data) {
                        $('#chat_fullscreen').html(data);
                        scrollToBottomFunc();
                    }
                });
            }

            $(document).ready(function () {

                if (getCookie('chat_user') != '') {
                    var k = JSON.parse(getCookie('chat_user'));
                    var receiver_id = k.id;
                    var my_id = 0;

                    // Enable pusher logging - don't include this in production
                    Pusher.logToConsole = false;

                    var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
                        cluster: 'ap2',
                        forceTLS: true
                    });

                    var channel = pusher.subscribe('my-channel');
                    channel.bind('my-event', function (data) {
                        /*alert(JSON.stringify(data));*/
                        if (my_id == data.from && receiver_id == data.to) {
                            getMsg();
                        }
                    });
                }

            });
        </script>
    @endif

@endsection
@section('Agent_Login')
    <div class="row w-100 pb-2">
        <div class="col-2">
            @if(env('CHAT_MODULE') == 'yes')
                <div>
                    <div class="fabs">
                        <div class="chat d-none">
                            <div class="chat_header">
                                <div class="chat_option">
                                    <div class="header_img">
                                        <img src="{{asset(Storage::url('app/small.png'))}}"/>
                                    </div>
                                    <span id="chat_head" class="position-absolute pt-2">Agent</span>
                                </div>
                            </div>
                            <div class="msg_chat">
                                <div id="chat_fullscreen" class="chat_conversion chat_converse">
                                    <h3 class="text-center mt-5 pt-5">No Message Found.!</h3>
                                </div>
                                <div class="fab_field">
                                    <textarea id="chatSend" name="chat_message" placeholder="Send a message" class="chat_field chat_message"></textarea>
                                </div>
                            </div>
                            <div class="msg_form">
                                <div id="chat_fullscreen" class="chat_conversion chat_converse">
                                    <form class="pt-4" name="chat_form">
                                        <div class="form-group row mb-3 ml-md-2">
                                            <div class="col-sm-10 col-md-11">
                                                <div class="input-group input-group-merge input-group-alternative">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                                    </div>
                                                    <input type="text" id="chat_email" placeholder="{{ __('Enter You Email') }}" name="name" class="form-control" autofocus>
                                                </div>
                                                <div class="invalid-feedback d-block e_error">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4 ml-md-2">
                                            <div class="col-sm-12 col-md-7">
                                                <button class="btn btn-primary btn-sm" id="chat_frm_submit" type="button"><span>{{ __('Start Chat') }}</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a id="prime" class="fab"><i class="prime fas fa-envelope text-white"></i></a>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-10 text-right">
            <a href="{{route('faq')}}" class="btn btn-dark mt-2">{{ __('FAQ') }}</a>
            <a href="{{route('login')}}" class="btn btn-dark mt-2">{{ __('Agent Login') }}</a>
            <a href="{{route('search')}}" class="btn btn-dark mt-2">{{ __('Search Ticket') }}</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="col-lg-12 col-md-12">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
                <a class="btn btn-primary pull-left" href="{{url('/')}}">Back to Main</a>
                <div class="text-center text-muted mb-4">

                    <h1 class="mb-3 text-18">{{ $sbu_name }} : {{ __('Create Ticket') }}</h1>
                </div>
                <form method="post" action="{{route('home.store',$sbu_id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="name" required="" value="{{old('name')}}" placeholder="{{ __('Name') }}"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('name') }}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                </div>
                                <input class="form-control" type="email" name="email" required="" value="{{old('email')}}" placeholder="{{ __('Email') }}"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('email') }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="phone" required="" value="{{old('phone')}}" placeholder="{{ __('Cell Number') }}"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('phone') }}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="idno" required="" value="{{old('idno')}}" placeholder="{{ __('ID Number') }}"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('idno') }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <select class="form-control select2" name="gender" required="">
                                    <option value="">{{ __('Select Gender') }}</option>
                                        <option value="M"  @if(old('gender') == 'M') selected @endif> Male </option>
                                        <option value="F"  @if(old('gender') == 'F') selected @endif> Female </option>
                                </select>
                            </div>
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('gender') }}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <select class="form-control select2" name="qualification" required="">
                                    <option value="">{{ __('Select Qualification') }}</option>
                                        <option value="O-Level" @if(old('qualification') == 'O-Level') selected @endif > O-Level </option>
                                        <option value="A-Level" @if(old('qualification') == 'A-Level') selected @endif> A-Level </option>
                                        <option value="Diploma" @if(old('qualification') == 'Diploma') selected @endif> Diploma</option>
                                        <option value="Degree" @if(old('qualification') == 'Degree') selected @endif> Degree</option>
                                        <option value="Masters" @if(old('qualification') == 'Masters') selected @endif> Masters</option>
                                </select>
                            </div>
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('qualification') }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="acc_number" required="" value="{{old('acc_number')}}" placeholder="{{ __('Account Number') }}"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('acc_number') }}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="branch" required="" value="{{old('branch')}}" placeholder="{{ __('Customer Branch') }}"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('branch') }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <select class="form-control select2" name="category" required="">
                                    <option value="">{{ __('Select Category') }}</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if(old('category') == $category->id) selected @endif>{{$category->name}}</option>
                                    @endforeach
                                    <option value="0">Other</option>
                                </select>
                            </div>
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('category') }}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-copy-04"></i></span>
                                </div>
                                <input class="form-control" type="text" name="subject" value="{{old('subject')}}" placeholder="{{ __('Subject') }}"/>
                            </div>

                            <div class="invalid-feedback d-block">
                                {{ $errors->first('subject') }}

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12">

                            <textarea name="description" class="form-control" placeholder="{{ __('Description') }}">{{old('description')}}</textarea>
                            <div class="invalid-feedback  d-block">
                                {{ $errors->first('description') }}

                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12">
                            <label class="form-control-label">{{ __('Attachments') }}</label>
                            <div class="custom-file">
                                <input type="file" multiple="" name="attachments[]" class="custom-file-input">
                                <label class="custom-file-label" for="customFileLang">{{ __('Select file') }}</label>
                                <div class="invalid-feedback d-block">
                                    {{ $errors->first('attachments') }}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-3 text-center">
                        <input type="hidden" name="status" value="In Progress"/>
                        <button class="btn btn-rounded btn-primary mt-2">{{ __('Create Ticket') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
