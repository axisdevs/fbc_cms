@extends('layouts.auth-master')
@section('title')
    {{ __('Login') }}
@endsection
@section('Agent_Login')
    <div style="text-align: right;float: right;width:100%;margin-right: 15px;margin-bottom: 10px; ">
        <a href="{{route('faq')}}" class="btn btn-dark mt-2">{{ __('FAQ') }}</a>
        <a href="{{route('main')}}" class="btn btn-dark mt-2">{{ __('Create Ticket') }}</a>
        <a href="{{route('search')}}" class="btn btn-dark mt-2">{{ __('Search Ticket') }}</a>
    </div>
@endsection
@section('content')
    <div class="col-lg-5 col-md-7">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                    <small>SIGN IN</small>
                </div>
                <form method="post">
                    @csrf
                    @if(session()->has('info'))
                        <div class="alert alert-primary">
                            {{ session()->get('info') }}
                        </div>
                    @endif
                    @if(session()->has('status'))
                        <div class="alert alert-info">
                            {{ session()->get('status') }}
                        </div>
                    @endif
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                            </div>
                            <input id="email" class="form-control pl-2" type="email" name="email" value="{{ old('email') }}" placeholder="Enter your email" autofocus/>
                        </div>
                        <div class="invalid-feedback d-block">
                            {{ $errors->first('email') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                            </div>
                            <input id="password" class="form-control pl-2" type="password" name="password" placeholder="Enter your password"/>
                        </div>
                        <div class="invalid-feedback">
                            {{ $errors->first('password') }}
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-primary mt-2">{{ __('Sign In') }}</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <div class="text-center">
                    <a href="{{ route('password.request') }}" class="text-light"><small>Forgot password?</small></a>
                </div>
            </div>
        </div>
    </div>

@endsection
