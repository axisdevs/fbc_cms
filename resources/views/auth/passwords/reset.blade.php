@extends('layouts.auth-master')
@section('title')
    {{ __('Set a New Password') }}
@endsection
@section('Agent_Login')
    <div style="text-align: right;float: right;width:100%;margin-right: 15px;margin-bottom: 10px; ">
        <a href="{{route('faq')}}" class="btn btn-dark mt-2">{{ __('FAQ') }}</a>
        <a href="{{route('home')}}" class="btn btn-dark mt-2">{{ __('Create Ticket') }}</a>
    </div>
@endsection
@section('content')
    <div class="col-lg-5 col-md-7">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                    <h2 class="mb-3 text-18">{{ __('Set a New Password') }}</h2>
                </div>
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                            </div>
                            <input id="email" class="form-control" placeholder="{{ __('Email Address') }}" type="email" name="email" value="{{ old('email') }}" autofocus>
                        </div>
                        <div class="invalid-feedback d-block">
                            {{ $errors->first('email') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                            </div>
                            <input id="password" type="password" class="form-control" name="password" placeholder="{{ __('Password') }}"/>
                        </div>
                        <div class="invalid-feedback d-block">
                            {{ $errors->first('password') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                            </div>
                            <input id="password_confirmation" type="password" placeholder="{{ __('Confirm Password') }}" class="form-control form-control-rounded {{ $errors->has('password_confirmation') ? ' is-invalid': '' }}" name="password_confirmation">
                        </div>
                        <div class="invalid-feedback d-block">
                            {{ $errors->first('password_confirmation') }}
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block btn-rounded mt-3">{{ __('Set a New Password') }}</button>
                </form>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <div class="text-center">
                    <a href="{{ route('login') }}" class="text-light"><small>{{ __('Sign In') }}</small></a>
                </div>
            </div>
        </div>
    </div>
@endsection
