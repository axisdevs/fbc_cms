@extends('layouts.auth-master')
@section('title')
    {{ __('Ticket') }} - {{$ticket->ticket_id}}
@endsection
@section('style')
    <style>


        @media (max-width: 767px) {
            .auth-layout-wrap .auth-content {
                min-width: 100%;
            }
        }

        @media (min-width: 768px) {
            .auth-layout-wrap .auth-content {
                min-width: 90%;
            }
        }

        @media (min-width: 1024px) {
            .auth-layout-wrap .auth-content {
                min-width: 50%;
            }
        }
    </style>
@endsection
@section('scripts')
    <script src="//cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('reply_description');
    </script>
@endsection

@section('Agent_Login')
    <div style="text-align: right;float: right;width:100%;margin-right: 15px;margin-bottom: 10px; ">
        <a href="{{route('faq')}}" class="btn btn-dark mt-2">{{ __('FAQ') }}</a>
    </div>
@endsection

@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h1 class="mb-3 text-18">{{ __('Ticket') }} - {{$ticket->ticket_id}}</h1>
            </div>
            <div class="card-body">
                @csrf
                <div class="card">
                    <div class="card-header"><h4>{{$ticket->name}} <small><small>{{$ticket->created_at->diffForHumans()}}</small></small></h4></div>
                    <div class="card-body">
                        <div>{!! $ticket->description !!}</div>
<br><br>

            <div>Status : <strong>{!! $ticket->status !!}</strong></div>
                        @php
                            $attachments=json_decode($ticket->attachments);
                        @endphp
                        @if(count($attachments))
                            <div class="m-1">
                                <b>{{ __('Attachments') }} :</b>
                                <ul class="list-group list-group-flush">

                                    @foreach($attachments as $index => $attachment)
                                        <li class="list-group-item">
                                            {{$attachment}}<a download="" href="{{ env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)}}" class="btn btn-outline-primary ml-1 p-2" title="{{ __('Download') }}"><i class="fa fa-download"></i></a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <br>
                @foreach($ticket->conversions as $conversion)
                    <div class="card">
                        <div class="card-header"><h4>{{$conversion->replyBy()->name}} <small><small>{{$conversion->created_at->diffForHumans()}}</small></small></h4></div>
                        <div class="card-body">
                            <div>{!! $conversion->description !!}</div>
                            @php
                                $attachments=json_decode($conversion->attachments);
                            @endphp
                            @if(count($attachments))
                                <div class="m-1">
                                    <b>{{ __('Attachments') }} :</b>
                                    <ul class="list-group list-group-flush">

                                        @foreach($attachments as $index => $attachment)
                                            <li class="list-group-item">
                                                {{$attachment}}<a download="" href="{{ env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)}}" class="btn btn-outline-primary ml-1 p-2" title="{{ __('Download') }}"><i class="fa fa-download"></i></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>
                @endforeach

                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('home.reply',$ticket->ticket_id)}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="require">{{ __('Description') }}</label>
                                <textarea name="reply_description" class="form-control">{{old('reply_description')}}</textarea>
                                <div class="invalid-feedback">
                                    {{ $errors->first('reply_description') }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="require">{{ __('Attachments') }}</label>
                                <div class="input-group file-group">
                                    <input type="file" multiple="" class="form-control" name="reply_attachments[]">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('reply_attachments') }}
                                    </div>
                                </div>
                            </div>
                            <div class="mt-3 text-center">
                                <input type="hidden" name="status" value="In Progress"/>
                                <button class="btn btn-rounded btn-primary mt-2">{{ __('Update Ticket') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
