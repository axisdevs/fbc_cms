<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title', 'Support System') &mdash; {{ env('APP_NAME') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="icon" href="{{asset(Storage::url('app/logo.png'))}}" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/nucleo/css/nucleo.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-toastr/toastr.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <!-- Page plugins -->

    <!-- Argon CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/argon.css?v=1.1.0') }}" type="text/css">

    @stack('css')
</head>

<body>
<!-- Sidenav -->
@include('admin.partials.sidebar')

<!-- Main content -->
<div class="main-content" id="panel">
    <!-- Topnav -->
@include('admin.partials.topnav')

<!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">@yield('title')</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            @yield('breadcrumb')
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        @yield('actions')
                    </div>
                </div>

                <!-- Card stats -->
                @yield('card')
            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">
    @yield('content')

    <!-- Footer -->
        @include('admin.partials.footer')
    </div>
</div>
<!-- Argon Scripts -->

{{-- Common Model --}}
<div id="commanModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modelCommanModelLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title" id="modelCommanModelLabel"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body pt-0"></div>
        </div>
    </div>
</div>
{{-- End Common Model --}}

<!-- Core -->
<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>
<script src="{{ asset('assets/vendor/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-toastr/toastr.min.js') }}"></script>

{{--Pusher JS--}}
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>

<!-- Optional JS -->
@yield('scripts')

<script src="{{ asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

<!-- Argon JS -->
<script src="{{ asset('assets/js/argon.js?v=1.1.0') }}"></script>

<script src="{{ asset('assets/js/custom.js') }}"></script>

{{-- Toaster Checker --}}
@if($message = Session::get('success'))
    <script>show_msg('Success', '{!! $message !!}', 'success');</script>
    {{ Session::forget('success') }}
@endif
@if($message = Session::get('error'))
    <script>show_msg('Error', '{!! $message !!}', 'error');</script>
    {{ Session::forget('error') }}
@endif

</body>
</html>
