@extends('layouts.auth-master')
@section('title')
    {{ __('Search Your Ticket') }}
@endsection
@section('Agent_Login')
    <div style="text-align: right;float: right;width:100%;margin-right: 15px;margin-bottom: 10px; ">
        <a href="{{route('faq')}}" class="btn btn-dark mt-2">{{ __('FAQ') }}</a>
        <a href="{{route('main')}}" class="btn btn-dark mt-2">{{ __('Create Ticket') }}</a>
    </div>
@endsection
@section('content')
    <div class="col-lg-5 col-md-7">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                    <h2 class="mb-3 text-18">{{ __('Search Your Ticket') }}</h2>
                </div>
                <form method="post">
                    @csrf
                    @if(session()->has('info'))
                        <div class="alert alert-primary">
                            {{ session()->get('info') }}
                        </div>
                    @endif
                    @if(session()->has('status'))
                        <div class="alert alert-info">
                            {{ session()->get('status') }}
                        </div>
                    @endif
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                            </div>
                            <input id="ticket_id" class="form-control {{ $errors->has('ticket_id') ? ' is-invalid' : '' }}" type="text" name="ticket_id" value="{{ old('ticket_id') }}" placeholder="{{ __('Ticket Number') }}" autofocus/>
                        </div>
                        <div class="invalid-feedback">
                            {{ $errors->first('ticket_id') }}
                        </div>
                    </div>
                    <button class="btn btn-rounded btn-primary btn-block mt-2">{{ __('Search') }}</button>

                </form>
            </div>
        </div>
    </div>
@endsection
