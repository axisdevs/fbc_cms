@extends('layouts.admin-master')

@section('title')
    {{ __('Manage Users') }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i> {{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('actions')
    @can('create-users')
        <a href="{{route('admin.users.create')}}" class="btn btn-sm btn-neutral">{{ __('Add') }} <i class="fa fa-user-plus"></i></a>
    @endcan
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable-basic" class="table dataTable-collapse text-center">
                            <thead class="thead-light">
                            <tr>
                                {{-- <th scope="col">#</th> --}}
                                <th scope="col">{{ __('Action') }}</th>
                                <th scope="col">{{ __('Avatar') }}</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Email') }}</th>
                                <th scope="col">{{ __('Role') }}</th>
                                <th scope="col">{{ __('Business Unit') }}</th>
                                <th scope="col">{{ __('Escalation Level') }}</th>
                                <th scope="col">{{ __('Designated Point Person') }}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $index => $user)
                                <tr>
                                    {{-- <th scope="row">{{++$index}}</th> --}}
                                    <td>
                                        @can('edit-users')
                                            <a class="text-success mr-4" title="{{ __('Edit') }}" href="{{route('admin.users.edit',$user->id)}}"><i class="fa fa-pen font-weight-bold"></i></a>
                                        @endcan
                                        @can('edit-users')
                                            <a class="text-danger mr-2" title="{{ __('Delete') }}" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-{{$user->id}}').submit()):'');"><i class="fa fa-trash font-weight-bold"></i></a>
                                            <form method="post" id="user-form-{{$user->id}}" action="{{route('admin.users.destroy',$user->id)}}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        @endcan
                                    </td>
                                    <td><img alt="image" class="rounded-circle m-0 avatar-sm-table" width="35" src="{{$user->avatarlink}}"></td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td><span class="badge badge-primary">{{$user->roles[0]->name}}</span></td>
                                    <td>{{empty($user->businessUnit)?'No Business Unit':$user->businessUnit->sbu_name}}</td>
                                    <td class="text-left">{{empty($user->escGroup)?"No Level":"Level - ".$user->escGroup->Level." : ".$user->escGroup->Description}}</td>
                                    <td>{{$user->is_desgnated_point_person==1?"Yes":"No"}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
