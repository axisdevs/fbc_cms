@extends('layouts.admin-master')

@section('title')
    {{ __('Site Settings ') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h4>{{ __('Update Site Settings') }}</h4></div>
                <div class="card-body">
                    <br><br>
                    <form method="post" action="{{route('admin.setting')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Site Logo') }}</label>
                            <div class="col-sm-12 col-md-7">
                                <div class="row">
                                    <div class="col-sm-8 col-md-10">
                                        <input class="form-control @error('avatar') is-invalid @enderror" name="logo" type="file" id="logo">
                                        <span class="invalid-feedback">
                                            {{ $errors->first('logo') }}
                                        </span>
                                        <span>
                                            <small>{{ __('Please upload a valid png image file. Size of image should not be more than 1MB.') }}</small>
                                        </span>
                                    </div>
                                    <div class="col-sm-4 col-md-2">
                                        <img src="{{asset(Storage::url('app/logo.png'))}}" alt="logo" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="form-group row mb-4 {{ $errors->has('mail_driver') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_driver">
                                    {{ trans('installer_messages.environment.wizard.form.app_tabs.mail_driver_label') }}
                                </label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_driver" id="mail_driver" class="form-control" value="{{env('MAIL_DRIVER')}}" placeholder="{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_driver_placeholder') }}"/>
                                    @if ($errors->has('mail_driver'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('mail_driver') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('mail_host') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_host">{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_host_label') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_host" id="mail_host" class="form-control" value="{{env('MAIL_HOST')}}" placeholder="{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_host_placeholder') }}"/>
                                    @if ($errors->has('mail_host'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('mail_host') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('mail_port') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_port">{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_port_label') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="number" name="mail_port" id="mail_port" class="form-control" value="{{env('MAIL_PORT')}}" placeholder="{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_port_placeholder') }}"/>
                                    @if ($errors->has('mail_port'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('mail_port') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('mail_username') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_username">{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_username_label') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_username" id="mail_username" class="form-control" value="{{env('MAIL_USERNAME')}}" placeholder="{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_username_placeholder') }}"/>
                                    @if ($errors->has('mail_username'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('mail_username') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('mail_password') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_password">{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_password_label') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_password" id="mail_password" class="form-control" value="{{env('MAIL_PASSWORD')}}" placeholder="{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_password_placeholder') }}"/>
                                    @if ($errors->has('mail_password'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('mail_password') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('mail_encryption') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_encryption">{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_encryption_label') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_encryption" id="mail_encryption" class="form-control" value="{{env('MAIL_ENCRYPTION')}}" placeholder="{{ trans('installer_messages.environment.wizard.form.app_tabs.mail_encryption_placeholder') }}"/>
                                    @if ($errors->has('mail_encryption'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('mail_encryption') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('lang') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="lang">{{ __('Default Front Language') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <select name="lang" id="lang" class="form-control select2">
                                        @foreach($lang as $row)
                                            <option @if(env('CUR_LANG') == $row) selected @endif>{{$row}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('lang'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('lang') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('chat') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="chat">{{ __('Chat Module') }}</label>
                                <div class="col-sm-12 col-md-7 pt-2">
                                    <label class="custom-toggle">
                                        <input type="checkbox" name="chat" id="chat" @if(env('CHAT_MODULE') == 'yes') checked @endif>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="Off" data-label-on="On"></span>
                                    </label>
                                    @if ($errors->has('chat'))
                                        <span class="error-block">
                                            <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                            {{ $errors->first('chat') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{--Pusher Configration--}}
                            <div class="form-group row mb-4 {{ $errors->has('pusher_app_id') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="pusher_app_id">{{ __('Pusher App Id') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="pusher_app_id" id="pusher_app_id" class="form-control" value="{{env('PUSHER_APP_ID')}}" placeholder="{{ __('Pusher App Id') }}"/>
                                    @if ($errors->has('pusher_app_id'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('pusher_app_id') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('pusher_app_key') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="pusher_app_key">{{ __('Pusher App Key') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="pusher_app_key" id="pusher_app_key" class="form-control" value="{{env('PUSHER_APP_KEY')}}" placeholder="{{ __('Pusher App Key') }}"/>
                                    @if ($errors->has('pusher_app_key'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('pusher_app_key') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('pusher_app_secret') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="pusher_app_secret">{{ __('Pusher App Secret') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="pusher_app_secret" id="pusher_app_secret" class="form-control" value="{{env('PUSHER_APP_SECRET')}}" placeholder="{{ __('Pusher App Secret') }}"/>
                                    @if ($errors->has('pusher_app_secret'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('pusher_app_secret') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-4 {{ $errors->has('pusher_app_cluster') ? ' has-error ' : '' }}">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="pusher_app_cluster">{{ __('Pusher App Cluster') }}</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="pusher_app_cluster" id="pusher_app_cluster" class="form-control" value="{{env('PUSHER_APP_CLUSTER')}}" placeholder="{{ __('Pusher App Cluster') }}"/>
                                    @if ($errors->has('pusher_app_cluster'))
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first('pusher_app_cluster') }}
                                </span>
                                    @endif
                                </div>
                            </div>
                            {{--End Pusher Confirgation--}}
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button class="btn btn-primary"><span>{{ __('Save') }}</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
