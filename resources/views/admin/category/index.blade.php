@extends('layouts.admin-master')

@section('title')
    {{ __('Manage Category') }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i> {{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('actions')
    @can('create-category')
        <a href="{{route('admin.category.create')}}" class="btn btn-sm btn-neutral">{{ __('Add') }} <i class="fa fa-plus-circle"></i></a>
    @endcan
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable-basic" class="table dataTable-collapse text-center">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Color') }}</th>
                                <th scope="col">{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $index => $category)
                                <tr>
                                    <th scope="row">{{++$index}}</th>
                                    <td>{{$category->name}}</td>
                                    <td><span class="badge" style="background: {{$category->color}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                                    <td>
                                        @can('edit-category')
                                            <a class="text-success mr-4" title="{{ __('Edit') }}" href="{{route('admin.category.edit',$category->id)}}"><i class="fa fa-pen font-weight-bold"></i></a>
                                        @endcan
                                        @can('edit-category')
                                            <a class="text-danger mr-2" title="{{ __('Delete') }}" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-{{$category->id}}').submit()):'');"><i class="fa fa-trash font-weight-bold"></i></a>
                                            <form method="post" id="user-form-{{$category->id}}" action="{{route('admin.category.destroy',$category->id)}}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
