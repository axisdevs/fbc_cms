@extends('layouts.admin-master')

@section('title')
    {{ __('Manage Faq') }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i> {{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('actions')
    @can('create-faq')
        <a href="{{route('admin.faq.create')}}" class="btn btn-sm btn-neutral">{{ __('Add') }} <i class="fa fa-plus-circle"></i></a>
    @endcan
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable-basic" class="table dataTable-collapse text-center">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Title') }}</th>
                                <th scope="col">{{ __('Description') }}</th>
                                <th scope="col">{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($faqs as $index => $faq)
                                <tr>
                                    <th scope="row">{{++$index}}</th>

                                    <td>{{$faq->title}}</td>
                                    <td>{{$faq->description}}</td>
                                    <td>
                                        @can('edit-faq')
                                            <a class="text-success mr-4" title="{{ __('Edit') }}" href="{{route('admin.faq.edit',$faq->id)}}"><i class="fa fa-pen font-weight-bold"></i></a>
                                        @endcan
                                        @can('edit-faq')
                                            <a class="text-danger mr-2" title="{{ __('Delete') }}" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-{{$faq->id}}').submit()):'');"><i class="fa fa-trash font-weight-bold"></i></a>
                                            <form method="post" id="user-form-{{$faq->id}}" action="{{route('admin.faq.destroy',$faq->id)}}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
