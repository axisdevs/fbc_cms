@extends('layouts.admin-master')

@section('title')
    {{ __('Business Units') }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i> {{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('actions')
    @can('create-users')
        <a href="{{route('admin.users.create')}}" class="btn btn-sm btn-neutral">{{ __('Add') }} <i class="fa fa-user-plus"></i></a>
    @endcan
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable-basic" class="table dataTable-collapse text-center">
                            <thead class="thead-light">
                            <tr>
                                {{-- <th scope="col">#</th> --}}
                                <th scope="col">{{ __('Action') }}</th>
                                <th scope="col">{{ __('Business Unit Name') }}</th>
                                <th scope="col">{{ __('Description') }}</th>
                                <th scope="col">{{ __('Max Required Exec Time') }}</th>
                                <th scope="col">{{ __('Created Date') }}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($buss_units as $index => $buss_unit)
                                <tr>
                                    {{-- <th scope="row">{{++$index}}</th> --}}
                                    <td>
                                        {{-- @can('edit-users')
                                            <a class="text-success mr-4" title="{{ __('Edit') }}" href="{{route('admin.users.edit',$user->id)}}"><i class="fa fa-pen font-weight-bold"></i></a>
                                        @endcan
                                        @can('edit-users')
                                            <a class="text-danger mr-2" title="{{ __('Delete') }}" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-{{$user->id}}').submit()):'');"><i class="fa fa-trash font-weight-bold"></i></a>
                                            <form method="post" id="user-form-{{$user->id}}" action="{{route('admin.users.destroy',$user->id)}}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        @endcan --}}
                                        <a class="text-success mr-4" title="{{ __('Edit') }}" href=""><i class="fa fa-pen font-weight-bold"></i></a>
                                    </td>
                                    {{-- <td><img alt="image" class="rounded-circle m-0 avatar-sm-table" width="35" src="{{$user->avatarlink}}"></td> --}}
                                    <td class="text-left">{{$buss_unit->sbu_name}}</td>
                                    <td class="text-left">{{$buss_unit->sbu_description}}</td>
                                    <td class="text-left">{{$buss_unit->max_allowed_resolution_time}}</td>
                                    <td><span class="badge badge-primary">{{date('d M y',strtotime($buss_unit->max_allowed_resolution_time))}}</span></td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
