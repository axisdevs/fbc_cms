@extends('layouts.admin-master')

@section('title')
    {{ __('Dashboard - ') }} {{$sbu_name}}
@endsection

@section('scripts')
    <script src="{{ asset('assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/chart.js/dist/Chart.extension.js') }}"></script>
    <script>
        $(document).ready(function () {
            var $chart = $('#chartBar');
            if ($chart.length) {
                init($chart);

                function init($this) {
                    var salesChart = new Chart($this, {
                        type: 'line',
                        options: {
                            scales: {
                                yAxes: [{
                                    gridLines: {
                                        color: Charts.colors.gray[700],
                                        zeroLineColor: Charts.colors.gray[700]
                                    },
                                    ticks: {
                                        callback: function (data) {
                                            return Number(data);
                                        },
                                        stepSize: 1
                                    }
                                }]
                            }
                        },
                        data: {
                            labels: {!! json_encode(array_keys($monthData)) !!},
                            datasets: [{
                                label: 'Tickets',
                                data: {!! json_encode(array_values($monthData)) !!}
                            }]
                        }
                    });
                    $this.data('chart', salesChart);
                };
            }

            var $pie_chart = $('#category_pie');
            if ($pie_chart.length) {
                function init($this) {
                    var randomScalingFactor = function () {
                        return Math.round(Math.random() * 100);
                    };
                    var pieChart = new Chart($this, {
                        type: 'pie',
                        data: {
                            labels:{!! json_encode($chartData['name']) !!},
                            datasets: [{
                                data: {!! json_encode($chartData['value']) !!},
                                backgroundColor: {!! json_encode($chartData['color']) !!},
                                label: 'Dataset 1'
                            }],
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            }
                        }
                    });

                    $this.data('chart', pieChart);
                };

                init($pie_chart);
            }
        });
    </script>
@endsection

<!-- CARD ICON -->
@section('card')
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{ __('Categories') }}</h5>
                            <span class="h2 font-weight-bold mb-0">{{$categories}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                <i class="ni ni-active-40"></i>
                            </div>
                        </div>
                    </div>
                    {{--<p class="mt-3 mb-0 text-sm">
                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                        <span class="text-nowrap">Since last month</span>
                    </p>--}}
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{ __('Open Tickets') }}</h5>
                            <span class="h2 font-weight-bold mb-0">{{$open_ticket}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                <i class="ni ni-chart-pie-35"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{ __('Closed Tickets') }}</h5>
                            <span class="h2 font-weight-bold mb-0">{{$close_ticket}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                <i class="ni ni-money-coins"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{ __('Total Agents') }}</h5>
                            <span class="h2 font-weight-bold mb-0">{{$agents}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                <i class="ni ni-chart-bar-32"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-8">
            <div class="card bg-default">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="h3 text-white mb-0">{{ __('This Year Tickets') }}</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="chartBar" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="h3 mb-0">{{ __('Tickets by Category') }}</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="category_pie" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
