@extends('layouts.admin-master')

@section('title')
    {{ $category }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i> {{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('actions')
    @can('create-tickets')
        <a href="{{route('admin.tickets.create')}}" class="btn btn-sm btn-neutral">{{ __('Add') }} <i class="fa fa-plus-circle"></i></a>
    @endcan
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if(session()->has('ticket_id') || session()->has('smtp_error'))
                <div class="alert alert-primary">
                    @if(session()->has('ticket_id'))
                        {!! Session::get('ticket_id') !!}
                        {{ Session::forget('ticket_id') }}
                    @endif
                    @if(session()->has('smtp_error'))
                        {!! Session::get('smtp_error') !!}
                        {{ Session::forget('smtp_error') }}
                    @endif
                </div>
            @endif
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable-basic" class="table dataTable-collapse text-center">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Ticket ID') }}</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Email') }}</th>
                                <th scope="col">{{ __('Subject') }}</th>

                                <th scope="col">{{ __('Channel') }}</th>
                                <th scope="col">{{ __('Status') }}</th>
                                <th scope="col">{{ __('Assignee') }}</th>
                                <th scope="col">{{ __('Resolution Stage') }}</th>
                                <th scope="col">{{ __('Created') }}</th>
                                <th scope="col">{{ __('Expected Close Date') }}</th>
                                <th scope="col">{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tickets as $index => $ticket)
                                <tr>
                                    <th scope="row">{{++$index}}</th>
                                    <td>{{$ticket->ticket_id}}</td>
                                    <td>{{$ticket->name}}</td>
                                    <td>{{$ticket->email}}</td>
                                    <td>{{$ticket->subject}}</td>
                                    {{-- <td><span class="badge badge-success" style="background: {{$ticket->color}}">{{$ticket->category_name}}</span></td> --}}
                                    <td>{{$ticket->channel}}</td>
                                    <td><span class="badge @if($ticket->status == 'In Progress')badge-warning @elseif($ticket->status == 'On Hold') badge-danger @else badge-success @endif">{{__($ticket->status)}}</span></td>
                                    <td>{{$ticket->ticket_assignee->name}}</td>
                                    <td>{{$ticket->ticket_level->Description}}</td>
                                    <td>{{$ticket->created_at->diffForHumans()}}</td>
                                    <td>{{date('d M y', strtotime($ticket->must_close_at))}}</td>
                                    <td>
                                        @can('reply-tickets')
                                            <a class="text-primary mr-4 " title="{{ __('Update Ticket') }}" href="{{route('admin.tickets.edit',$ticket->id)}}"><i class="fa fa-reply font-weight-bold"></i></a>
                                        @endcan
                                        @can('edit-tickets')
                                            <a class="text-danger mr-2" title="{{ __('Delete') }}" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-{{$ticket->id}}').submit()):'');"><i class="fa fa-trash font-weight-bold"></i></a>
                                            <form method="post" id="user-form-{{$ticket->id}}" action="{{route('admin.tickets.destroy',$ticket->id)}}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
