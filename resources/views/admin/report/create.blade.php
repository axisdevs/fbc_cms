@extends('layouts.admin-master')

@section('title')
    {{ __('Report By Channel') }}
@endsection

@section('breadcrumb')
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i> {{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.users') }}">{{ __('Report') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h4>{{ __('Report By Channel') }}</h4></div>
                <div class="card-body">
                    <br><br>
                    <form method="post" class="needs-validation" action="{{route('admin.report.check')}}" enctype="multipart/form-data">
                        @csrf



                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3">{{ __('Channel') }}</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control select2" name="channel" required>
                                   <option>Please select channel</option>
                                   <option value='Web'>CMS Site - Web</option>
                                   <option value="Twitter">From Twitter</option>
                                   <option value="Whatsapp">From Whatsapp</option>
                                   <option value="Facebook">From Facebook</option>
                                </select>
                            </div>
                        </div>




                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button class="btn btn-primary"><span>{{ __('Generate') }}</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
