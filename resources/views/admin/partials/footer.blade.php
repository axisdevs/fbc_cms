<footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-12">
            <div class="copyright text-center text-lg-right text-muted">
                &copy; 2019 <a href="#" class="font-weight-bold ml-1">{{env('APP_NAME')}}</a> {{ __('All rights reserved') }}
            </div>
        </div>
    </div>
</footer>
