<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="">
                <img src="{{asset(Storage::url('app/logo.png'))}}" class="navbar-brand-img" alt="">
            </a>
            <div class="ml-auto">
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('*dashboard*') ? ' active' : '' }}" href="{{ route('admin.dashboard') }}">
                            <i class="ni ni-shop text-primary"></i>
                            <span class="nav-link-text">{{ __('Dashboard') }}</span>
                        </a>
                    </li>
                    @can('manage-users')
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('*users*') ? ' active' : '' }}" href="{{ route('admin.users') }}">
                                <i class="ni ni-single-02 text-info"></i>
                                <span class="nav-link-text">{{ __('Users') }}</span>
                            </a>
                        </li>
                    @endcan
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('*ticket*') ? ' active' : '' }}" href="#navbar-maps" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                            <i class="ni ni-briefcase-24 text-dark"></i>
                            <span class="nav-link-text">{{ __('Ticket') }}</span>
                        </a>
                        <div class="collapse {{ request()->is('*ticket*') ? ' show' : '' }}" id="navbar-maps">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item {{ request()->is('*ticket') ? ' active' : '' }}">
                                    <a href="{{route('admin.tickets.index')}}" class="nav-link">
                                        {{ __('All') }}
                                    </a>
                                </li>
                                <li class="nav-item {{ request()->is('*ticket/in-progress') ? ' active' : '' }}">
                                    <a href="{{route('admin.tickets.index','in-progress')}}" class="nav-link">
                                        <span class="item-name">{{ __('In Progress') }}</span>
                                    </a>
                                </li>
                                <li class="nav-item {{ request()->is('*ticket/on-hold') ? ' active' : '' }}">
                                    <a href="{{route('admin.tickets.index','on-hold')}}" class="nav-link">
                                        <span class="item-name">{{ __('On Hold') }}</span>
                                    </a>
                                </li>
                                <li class="nav-item {{ request()->is('*ticket/closed') ? ' active' : '' }}">
                                    <a href="{{route('admin.tickets.index','closed')}}" class="nav-link">
                                        <span class="item-name">{{ __('Closed') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @can('manage-category')
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('*category*') ? ' active' : '' }}" href="{{ route('admin.category') }}">
                                <i class="ni ni-archive-2 text-green"></i>
                                <span class="nav-link-text">{{ __('Category') }}</span>
                            </a>
                        </li>
                    @endcan
                    @can('manage-faq')
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('*faq*') ? ' active' : '' }}" href="{{ route('admin.faq') }}">
                                <i class="ni ni-ungroup text-orange"></i>
                                <span class="nav-link-text">{{ __('FAQ') }}</span>
                            </a>
                        </li>
                    @endcan
                    @if(env('CHAT_MODULE') == 'yes')
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('*chat*') ? ' active' : '' }}" href="{{ route('admin.chats') }}">
                                <i class="ni ni-chat-round text-primary"></i>
                                <span class="nav-link-text">{{ __('Chat') }}</span>
                            </a>
                        </li>
                    @endif
                    @can('manage-setting')
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('*setting*') ? ' active' : '' }}" href="{{ route('admin.setting') }}">
                                <i class="ni ni-settings text-red"></i>
                                <span class="nav-link-text">{{ __('Setting') }}</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('*business_units*') ? ' active' : '' }}" href="{{ route('admin.buss_units') }}">
                                <i class="ni ni-books text-success"></i>
                                <span class="nav-link-text">{{ __('Business Units') }}</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('*channel-report*') ? ' active' : '' }}" href="#navbar-maps" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                                <i class="ni ni-chart-bar-32 text-dark"></i>
                                <span class="nav-link-text">{{ __('Reports') }}</span>
                            </a>
                            <div class="collapse {{ request()->is('*channel-report*') ? ' show' : '' }}" id="navbar-maps">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item {{ request()->is('*channel-report') ? ' active' : '' }}">
                                        <a href="{{route('admin.report.channel')}}" class="nav-link">
                                            {{ __('By Channel') }}
                                        </a>
                                    </li>
                                    <li class="nav-item {{ request()->is('*reports/category') ? ' active' : '' }}">
                                        <a href="{{route('admin.report.category')}}" class="nav-link">
                                            <span class="item-name">{{ __('By Category') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ request()->is('*reports/sentiment') ? ' active' : '' }}">
                                        <a href="{{route('admin.tickets.index','on-hold')}}" class="nav-link">
                                            <span class="item-name">{{ __('Sentiment Analysis') }}</span>
                                        </a>
                                    </li>
                                    {{-- <li class="nav-item {{ request()->is('*ticket/closed') ? ' active' : '' }}">
                                        <a href="{{route('admin.tickets.index','closed')}}" class="nav-link">
                                            <span class="item-name">{{ __('Closed') }}</span>
                                        </a>
                                    </li> --}}
                                </ul>
                            </div>
                        </li>
                    @endcan
                </ul>
            </div>
        </div>
    </div>
</nav>
