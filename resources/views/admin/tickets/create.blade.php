@extends('layouts.admin-master')

@section('title')
    {{ __('Create Ticket') }}
@endsection
@section('scripts')
    <script src="//cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i> {{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.tickets.index') }}">{{ __('Manage Ticket') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('content')
    <form action="{{route('admin.tickets.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <div class="card-header"><h4>{{ __('Ticket Information') }}</h4></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="require form-control-label">{{ __('Name') }}</label>
                                <input class="form-control" type="text" name="name" required="">
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="require form-control-label">{{ __('Email') }}</label>
                                <input class="form-control" type="email" name="email" required="">
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="require form-control-label">{{ __('Category') }}</label>
                                <select class="form-control select2" name="category" required="">
                                    <option value="">{{ __('Select Category') }}</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    {{ $errors->first('category') }}
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="require form-control-label">{{ __('Status') }}</label>
                                <select class="form-control select2" name="status" required="">
                                    <option value="In Progress">{{ __('In Progress') }}</option>
                                    <option value="On Hold">{{ __('On Hold') }}</option>
                                    <option value="Closed">{{ __('Closed') }}</option>
                                </select>
                                <div class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="require form-control-label">{{ __('Subject') }}</label>
                                <input class="form-control" type="text" name="subject" required="">
                                <div class="invalid-feedback">
                                    {{ $errors->first('subject') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="require form-control-label">{{ __('Description') }}</label>
                            <textarea name="description" class="form-control"></textarea>
                            <div class="invalid-feedback">
                                {{ $errors->first('description') }}
                            </div>
                        </div>
                    </div>

                    <footer class="card-footer text-right">
                        <a class="btn btn-secondary mr-2" href="{{route('admin.tickets.index')}}">{{ __('Cancel') }}</a>
                        <button class="btn btn-primary" type="submit" name="save">{{ __('Submit') }}</button>
                    </footer>
                </div>

            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-header"><h4>{{ __('Attachments') }}</h4></div>
                    <div class="card-body">
                        <div class="input-group file-group">
                            <input type="file" class="form-control" multiple="" name="attachments[]">
                            <div class="invalid-feedback">
                                {{ $errors->first('attachments') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
@endsection
