@extends('layouts.admin-master')

@section('title')
    {{ __('Reply Ticket') }} - {{ $ticket->ticket_id }}
@endsection
@section('scripts')
    <script src="//cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
        CKEDITOR.replace('reply_description');
        CKEDITOR.replace('note');
    </script>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i> {{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.tickets.index') }}">{{ __('Manage Ticket') }}</a></li>
        {{-- <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>--}}
    </ol>
@endsection

@section('actions')
    @can('edit-tickets')
        <button class="btn btn-sm btn-neutral" type="button" data-toggle="collapse" href="#ticket-info"><i class="fa fa-edit"></i> {{ __('Action Ticket') }}</button>
        {{-- <button class="btn btn-sm btn-danger" type="button" data-toggle="collapse" href="{{route('admin.tickets.escalate',$ticket->id)}}"><i class="fa fa-arrow-up"></i> {{ __('Escalate Ticket') }}</button> --}}
    @endcan
@endsection

@section('content')
    @can('edit-tickets')
        <form class="collapse" id="ticket-info" action="{{route('admin.tickets.update',$ticket->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-8">
                    <div class="card">
                        <div class="card-header"><h4>{{ __('Ticket Information') }}</h4></div>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="require form-control-label">{{ __('Name') }}</label>
                                    <input class="form-control" type="text" name="name" required="" value="{{ $ticket->name }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('name') }}
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="require form-control-label">{{ __('Email') }}</label>
                                    <input class="form-control" type="email" name="email" required="" value="{{ $ticket->email }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="require form-control-label">{{ __('Category') }}</label>
                                    <select class="form-control select2" name="category" required="">
                                        <option value="">{{ __('Select Category') }}</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" @if($ticket->category == $category->id) selected @endif>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('category') }}
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="require form-control-label">{{ __('Status') }}</label>
                                    <select class="form-control select2" name="status" required="">
                                        <option value="In Progress" @if($ticket->status == 'In Progress') selected @endif>{{ __('In Progress') }}</option>
                                        <option value="On Hold" @if($ticket->status == 'On Hold') selected @endif>{{ __('On Hold') }}</option>
                                        <option value="Closed" @if($ticket->status == 'Closed') selected @endif>{{ __('Closed') }}</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('status') }}
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <label class="require form-control-label">{{ __('Subject') }}</label>
                                    <input class="form-control" type="text" name="subject" required="" value="{{ $ticket->subject }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('subject') }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="require form-control-label">{{ __('Description') }}</label>
                                <textarea name="description" class="form-control">{{ $ticket->description }}</textarea>
                                <div class="invalid-feedback">
                                    {{ $errors->first('description') }}
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer text-right">
                            <a class="btn btn-secondary mr-2" href="{{route('admin.tickets.index')}}">{{ __('Cancel') }}</a>
                            <button class="btn btn-primary" type="submit">{{ __('Update') }}</button>
                        </footer>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-header"><h4>{{ __('Attachments') }}</h4></div>
                        <div class="card-body">
                            <div class="input-group file-group">
                                <input type="file" multiple="" name="attachments[]" class="form-control">
                                <div class="invalid-feedback">
                                    {{ $errors->first('attachments') }}
                                </div>
                            </div>
                            <div class="m-1">
                                <ul class="list-group list-group-flush">
                                    @php
                                        $attachments=json_decode($ticket->attachments);
                                    @endphp
                                    @foreach($attachments as $index => $attachment)
                                        <li class="list-group-item">
                                            {{$attachment}}<a download="" href="{{ env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)}}" class="btn btn-outline-primary ml-1 p-2" title="{{ __('Download') }}"><i class="fa fa-download"></i></a>
                                            <a class="text-danger ml-1" title="Delete" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-{{$index}}').submit()):'');"><i class="nav-icon i-Close-Window font-weight-bold"></i></a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @foreach($attachments as $index => $attachment)
            <form method="post" id="user-form-{{$index}}" action="{{route('admin.tickets.attachment.destroy',[$ticket->id,$index])}}">
                @csrf
                @method('DELETE')
            </form>
        @endforeach
    @endcan
    <br><br>
    <div class="row">

        <div class="col-8">

            <div class="card">
                <div class="card-header"><h4>{{$ticket->name}} <small><small>{{$ticket->created_at->diffForHumans()}}</small></small></h4></div>
                <div class="card-body">
                    <div>{!! $ticket->description !!}</div>
                    @php
                        $attachments=json_decode($ticket->attachments);
                    @endphp
                    @if(count($attachments))
                        <div class="m-1">
                            <b>{{ __('Attachments') }} :</b>
                            <ul class="list-group list-group-flush">
                                @foreach($attachments as $index => $attachment)
                                    <li class="list-group-item">
                                        {{$attachment}}<a download="" href="{{ env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)}}" class="btn btn-outline-primary ml-1 p-2" title="{{ __('Download') }}"><i class="fa fa-download"></i></a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <br>

            @foreach($ticket->conversions as $conversion)
                <div class="card">
                    <div class="card-header"><h4>{{$conversion->replyBy()->name}} <small><small>{{$conversion->created_at->diffForHumans()}}</small></small></h4></div>
                    <div class="card-body">
                        <div>{!! $conversion->description !!}</div>
                        @php
                            $attachments=json_decode($conversion->attachments);
                        @endphp
                        @if(count($attachments))
                            <div class="m-1">
                                <b>Attachments :</b>
                                <ul class="list-group list-group-flush">
                                    @foreach($attachments as $index => $attachment)
                                        <li class="list-group-item">
                                            {{$attachment}}<a download="" href="{{ env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)}}" class="btn btn-outline-primary ml-1 p-2" title="{{ __('Download') }}"><i class="fa fa-download"></i></a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <br>
            @endforeach
        </div>
        @can('reply-tickets')
            <div class="col-4">
                <div class="card">
                    <div class="card-header"><h4>{{ __('Escalate') }}</h4></div>
                    <form method="post" action="{{route('admin.escalation.update',$ticket->id)}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">

                            <div class="form-group">
                                <label class="require form-control-label">{{ __('Escalate Resource') }}</label>
                                <select class="form-control select2" name="issue_escalated_to" required="">
                                    <option>Pick resource</option>
                                    @foreach($escalate_resource as $rsc)
                                <option value="{{$rsc->id}}">{{$rsc->name}} </option>
                                @endforeach

                                </select>
                                <div class="invalid-feedback">
                                    {{ $errors->first('resource') }}
                                </div>
                            </div>
                            <div class="form-group file-group">
                                <label class="require form-control-label">{{ __('Attachments') }}</label>
                                <input type="file" class="form-control" multiple="" name="reply_attachments[]">
                                <div class="invalid-feedback">
                                    {{ $errors->first('reply_attachments') }}
                                </div>
                            </div>
                        </div>

                        <footer class="card-footer text-right">
                            <button class="btn btn-primary" type="submit">{{ __('Escalate') }}</button>
                        </footer>
                    </form>
                </div>
                <div class="card mt-4">
                    <div class="card-header"><h4>{{ __('Note') }}</h4></div>
                    <form method="post" action="{{route('admin.note.store',$ticket->id)}}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <textarea name="note" class="form-control">{{$ticket->note}}</textarea>
                                <div class="invalid-feedback">
                                    {{ $errors->first('note') }}
                                </div>
                            </div>
                        </div>

                        <footer class="card-footer text-right">
                            <button class="btn btn-primary" type="submit">{{ __('Add Note') }}</button>
                        </footer>
                    </form>
                </div>
            </div>
        @endcan
    </div>



@endsection
