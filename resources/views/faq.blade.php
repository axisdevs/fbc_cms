@extends('layouts.auth-master')
@section('title')
    {{ __('FAQ') }}
@endsection
@section('style')
    <style>
        @media (max-width: 767px) {
            .auth-layout-wrap .auth-content {
                min-width: 100%;
            }
        }

        @media (min-width: 768px) {
            .auth-layout-wrap .auth-content {
                min-width: 90%;
            }
        }

        @media (min-width: 1024px) {
            .auth-layout-wrap .auth-content {
                min-width: 50%;
            }
        }
    </style>
@endsection
@section('Agent_Login')
    <div style="text-align: right;float: right;width:100%;margin-right: 15px;margin-bottom: 10px; ">
        <a href="{{route('main')}}" class="btn btn-dark mt-2">{{ __('Create Ticket') }}</a>
    </div>
@endsection
@section('content')

    <div class="col-lg-5 col-md-7">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                    <h2 class="mb-3 text-18">{{ __('FAQ') }}</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            @foreach($faqs as $index => $faq)
                                <div class="card">
                                    <div class="card-header p-1 collapsed" id="headingOne" data-toggle="collapse" data-target="#collapse-{{$faq->id}}" aria-expanded="false" aria-controls="collapseOne">
                                        <h4 class="mb-0 py-2 pl-3 text-primary">
                                            {{$faq->title}}
                                        </h4>
                                    </div>

                                    <div id="collapse-{{$faq->id}}" class="collapse @if($index == 0) show @endif" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                                        <div class="card-body">
                                            {{$faq->description}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
