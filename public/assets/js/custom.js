/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";

$(document).on('click', 'a[data-ajax-popup="true"], button[data-ajax-popup="true"]', function () {
    var title = $(this).data('title');
    var size = ($(this).data('size') == '') ? 'md' : $(this).data('size');
    var url = $(this).data('url');

    $("#commanModel .modal-title").html(title);
    $("#commanModel .modal-dialog").addClass('modal-' + size);
    $("#commanModel").modal('show');

    $.get(url, {}, function (data) {
        $('#commanModel .modal-body').html(data);
    });

});

$(document).ready(function () {
    /*if ($('#datatable-basic').length > 0) {
        $("#datatable-basic").DataTable();
    }*/

    if ($(".select2").length > 0) {
        $('.select2').select2({
            minimumResultsForSearch: -1
        });
    }
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function show_msg(title, message, status) {
    toastr[status](message, title)
}
