<?php $__env->startSection('title'); ?>
    <?php echo e(__('Create Category')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.category')); ?>"><?php echo e(__('Manage Category')); ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $__env->yieldContent('title'); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h4><?php echo e(__('Add a New Category')); ?></h4></div>
                <div class="card-body">
                    <br><br>
                    <form method="post" class="needs-validation" action="<?php echo e(route('admin.category.store')); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Name')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" placeholder="<?php echo e(__('Name of the Category')); ?>" name="name" class="form-control <?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('name')); ?>" autofocus>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('name')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Color')); ?></label>
                            <div class="col-sm-12 col-md-1">
                                <input type="color" name="color" class="form-control <?php echo e($errors->has('color') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('color')); ?>">
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('color')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button class="btn btn-primary"><span><?php echo e(__('Add')); ?></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/category/create.blade.php ENDPATH**/ ?>