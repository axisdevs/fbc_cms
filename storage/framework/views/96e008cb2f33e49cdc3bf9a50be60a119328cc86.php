<?php $__env->startSection('title'); ?>
    <?php echo e(__('Manage Category')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $__env->yieldContent('title'); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('actions'); ?>
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create-category')): ?>
        <a href="<?php echo e(route('admin.category.create')); ?>" class="btn btn-sm btn-neutral"><?php echo e(__('Add')); ?> <i class="fa fa-plus-circle"></i></a>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable-basic" class="table dataTable-collapse text-center">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"><?php echo e(__('Name')); ?></th>
                                <th scope="col"><?php echo e(__('Color')); ?></th>
                                <th scope="col"><?php echo e(__('Action')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <th scope="row"><?php echo e(++$index); ?></th>
                                    <td><?php echo e($category->name); ?></td>
                                    <td><span class="badge" style="background: <?php echo e($category->color); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                                    <td>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit-category')): ?>
                                            <a class="text-success mr-4" title="<?php echo e(__('Edit')); ?>" href="<?php echo e(route('admin.category.edit',$category->id)); ?>"><i class="fa fa-pen font-weight-bold"></i></a>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit-category')): ?>
                                            <a class="text-danger mr-2" title="<?php echo e(__('Delete')); ?>" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-<?php echo e($category->id); ?>').submit()):'');"><i class="fa fa-trash font-weight-bold"></i></a>
                                            <form method="post" id="user-form-<?php echo e($category->id); ?>" action="<?php echo e(route('admin.category.destroy',$category->id)); ?>">
                                                <?php echo csrf_field(); ?>
                                                <?php echo method_field('DELETE'); ?>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/category/index.blade.php ENDPATH**/ ?>