<?php $__env->startSection('title'); ?>
    <?php echo e(__('Manage Users')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $__env->yieldContent('title'); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('actions'); ?>
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create-users')): ?>
        <a href="<?php echo e(route('admin.users.create')); ?>" class="btn btn-sm btn-neutral"><?php echo e(__('Add')); ?> <i class="fa fa-user-plus"></i></a>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable-basic" class="table dataTable-collapse text-center">
                            <thead class="thead-light">
                            <tr>
                                
                                <th scope="col"><?php echo e(__('Action')); ?></th>
                                <th scope="col"><?php echo e(__('Avatar')); ?></th>
                                <th scope="col"><?php echo e(__('Name')); ?></th>
                                <th scope="col"><?php echo e(__('Email')); ?></th>
                                <th scope="col"><?php echo e(__('Role')); ?></th>
                                <th scope="col"><?php echo e(__('Business Unit')); ?></th>
                                <th scope="col"><?php echo e(__('Escalation Level')); ?></th>
                                <th scope="col"><?php echo e(__('Designated Point Person')); ?></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    
                                    <td>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit-users')): ?>
                                            <a class="text-success mr-4" title="<?php echo e(__('Edit')); ?>" href="<?php echo e(route('admin.users.edit',$user->id)); ?>"><i class="fa fa-pen font-weight-bold"></i></a>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit-users')): ?>
                                            <a class="text-danger mr-2" title="<?php echo e(__('Delete')); ?>" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-<?php echo e($user->id); ?>').submit()):'');"><i class="fa fa-trash font-weight-bold"></i></a>
                                            <form method="post" id="user-form-<?php echo e($user->id); ?>" action="<?php echo e(route('admin.users.destroy',$user->id)); ?>">
                                                <?php echo csrf_field(); ?>
                                                <?php echo method_field('DELETE'); ?>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                    <td><img alt="image" class="rounded-circle m-0 avatar-sm-table" width="35" src="<?php echo e($user->avatarlink); ?>"></td>
                                    <td><?php echo e($user->name); ?></td>
                                    <td><?php echo e($user->email); ?></td>
                                    <td><span class="badge badge-primary"><?php echo e($user->roles[0]->name); ?></span></td>
                                    <td><?php echo e(empty($user->businessUnit)?'No Business Unit':$user->businessUnit->sbu_name); ?></td>
                                    <td class="text-left"><?php echo e(empty($user->escGroup)?"No Level":"Level - ".$user->escGroup->Level." : ".$user->escGroup->Description); ?></td>
                                    <td><?php echo e($user->is_desgnated_point_person==1?"Yes":"No"); ?></td>

                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/users/index.blade.php ENDPATH**/ ?>