<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $__env->yieldContent('title', 'Home'); ?> &mdash; <?php echo e(env('APP_NAME')); ?></title>
    <!-- Favicon -->
    <link rel="icon" href="<?php echo e(asset(Storage::url('app/logo.png'))); ?>" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/nucleo/css/nucleo.css')); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/select2/dist/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/bootstrap-toastr/toastr.min.css')); ?>">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/argon.css?v=1.1.0')); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/custom.css')); ?>" type="text/css">

    <?php echo $__env->yieldContent('style'); ?>
    
</head>

<body class="bg-default">
<div class="main-content">

    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-6 pt-lg-4">
        <div class="container">
            <div class="header-body text-center mb-7">
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>

    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
    </div>
</div>

<!-- Footer -->
<footer class="py-2" id="footer-main">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-xl-between">
            <div class="col-lg-12 col-xl-12">
                <?php echo $__env->yieldContent('Agent_Login'); ?>
            </div>
        </div>
    </div>
</footer>


<script src="<?php echo e(asset('assets/vendor/jquery/dist/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/js-cookie/js.cookie.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/select2/dist/js/select2.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/bootstrap-toastr/toastr.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/argon.js?v=1.1.0')); ?>"></script>


<script src="https://js.pusher.com/5.0/pusher.min.js"></script>


<script src="<?php echo e(asset('assets/js/custom.js')); ?>"></script>
<?php echo $__env->yieldContent('scripts'); ?>



<?php if($message = Session::get('success')): ?>
    <script>show_msg('Success', '<?php echo $message; ?>', 'success');</script>
    <?php echo e(Session::forget('success')); ?>

<?php endif; ?>
<?php if($message = Session::get('error')): ?>
    <script>show_msg('Error', '<?php echo $message; ?>', 'error');</script>
    <?php echo e(Session::forget('error')); ?>

<?php endif; ?>

</body>
</html>
<?php /**PATH C:\xampp\htdocs\support\resources\views/layouts/auth-master.blade.php ENDPATH**/ ?>