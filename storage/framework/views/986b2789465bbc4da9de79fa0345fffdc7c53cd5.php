<?php $__env->startSection('title'); ?>
    <?php echo e(__('Manage Tickets')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $__env->yieldContent('title'); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('actions'); ?>
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create-tickets')): ?>
        <a href="<?php echo e(route('admin.tickets.create')); ?>" class="btn btn-sm btn-neutral"><?php echo e(__('Add')); ?> <i class="fa fa-plus-circle"></i></a>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?php if(session()->has('ticket_id') || session()->has('smtp_error')): ?>
                <div class="alert alert-primary">
                    <?php if(session()->has('ticket_id')): ?>
                        <?php echo Session::get('ticket_id'); ?>

                        <?php echo e(Session::forget('ticket_id')); ?>

                    <?php endif; ?>
                    <?php if(session()->has('smtp_error')): ?>
                        <?php echo Session::get('smtp_error'); ?>

                        <?php echo e(Session::forget('smtp_error')); ?>

                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable-basic" class="table dataTable-collapse text-center">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"><?php echo e(__('Ticket ID')); ?></th>
                                <th scope="col"><?php echo e(__('Name')); ?></th>
                                <th scope="col"><?php echo e(__('Email')); ?></th>
                                <th scope="col"><?php echo e(__('Subject')); ?></th>
                                <th scope="col"><?php echo e(__('Category')); ?></th>
                                <th scope="col"><?php echo e(__('Status')); ?></th>
                                <th scope="col"><?php echo e(__('Assignee')); ?></th>
                                <th scope="col"><?php echo e(__('Resolution Stage')); ?></th>
                                <th scope="col"><?php echo e(__('Created')); ?></th>
                                <th scope="col"><?php echo e(__('Expected Close Date')); ?></th>
                                <th scope="col"><?php echo e(__('Action')); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $ticket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <th scope="row"><?php echo e(++$index); ?></th>
                                    <td><?php echo e($ticket->ticket_id); ?></td>
                                    <td><?php echo e($ticket->name); ?></td>
                                    <td><?php echo e($ticket->email); ?></td>
                                    <td><?php echo e($ticket->subject); ?></td>
                                    <td><span class="badge badge-success" style="background: <?php echo e($ticket->color); ?>"><?php echo e($ticket->category_name); ?></span></td>
                                    <td><span class="badge <?php if($ticket->status == 'In Progress'): ?>badge-warning <?php elseif($ticket->status == 'On Hold'): ?> badge-danger <?php else: ?> badge-success <?php endif; ?>"><?php echo e(__($ticket->status)); ?></span></td>
                                    <td><?php echo e($ticket->ticket_assignee->name); ?></td>
                                    <td><?php echo e($ticket->ticket_level->Description); ?></td>
                                    <td><?php echo e($ticket->created_at->diffForHumans()); ?></td>
                                    <td><?php echo e(date('d M y', strtotime($ticket->must_close_at))); ?></td>
                                    <td>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('reply-tickets')): ?>
                                            <a class="text-primary mr-4 " title="<?php echo e(__('Update Ticket')); ?>" href="<?php echo e(route('admin.tickets.edit',$ticket->id)); ?>"><i class="fa fa-reply font-weight-bold"></i></a>
                                        <?php endif; ?>
                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit-tickets')): ?>
                                            <a class="text-danger mr-2" title="<?php echo e(__('Delete')); ?>" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-<?php echo e($ticket->id); ?>').submit()):'');"><i class="fa fa-trash font-weight-bold"></i></a>
                                            <form method="post" id="user-form-<?php echo e($ticket->id); ?>" action="<?php echo e(route('admin.tickets.destroy',$ticket->id)); ?>">
                                                <?php echo csrf_field(); ?>
                                                <?php echo method_field('DELETE'); ?>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/tickets/index.blade.php ENDPATH**/ ?>