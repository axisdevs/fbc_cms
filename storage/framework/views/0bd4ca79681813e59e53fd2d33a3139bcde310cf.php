<?php $__env->startSection('title'); ?>
    <?php echo e(__('Dashboard - ')); ?> <?php echo e($sbu_name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('assets/vendor/chart.js/dist/Chart.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/vendor/chart.js/dist/Chart.extension.js')); ?>"></script>
    <script>
        $(document).ready(function () {
            var $chart = $('#chartBar');
            if ($chart.length) {
                init($chart);

                function init($this) {
                    var salesChart = new Chart($this, {
                        type: 'line',
                        options: {
                            scales: {
                                yAxes: [{
                                    gridLines: {
                                        color: Charts.colors.gray[700],
                                        zeroLineColor: Charts.colors.gray[700]
                                    },
                                    ticks: {
                                        callback: function (data) {
                                            return Number(data);
                                        },
                                        stepSize: 1
                                    }
                                }]
                            }
                        },
                        data: {
                            labels: <?php echo json_encode(array_keys($monthData)); ?>,
                            datasets: [{
                                label: 'Tickets',
                                data: <?php echo json_encode(array_values($monthData)); ?>

                            }]
                        }
                    });
                    $this.data('chart', salesChart);
                };
            }

            var $pie_chart = $('#category_pie');
            if ($pie_chart.length) {
                function init($this) {
                    var randomScalingFactor = function () {
                        return Math.round(Math.random() * 100);
                    };
                    var pieChart = new Chart($this, {
                        type: 'pie',
                        data: {
                            labels:<?php echo json_encode($chartData['name']); ?>,
                            datasets: [{
                                data: <?php echo json_encode($chartData['value']); ?>,
                                backgroundColor: <?php echo json_encode($chartData['color']); ?>,
                                label: 'Dataset 1'
                            }],
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            }
                        }
                    });

                    $this.data('chart', pieChart);
                };

                init($pie_chart);
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<!-- CARD ICON -->
<?php $__env->startSection('card'); ?>
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0"><?php echo e(__('Categories')); ?></h5>
                            <span class="h2 font-weight-bold mb-0"><?php echo e($categories); ?></span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                <i class="ni ni-active-40"></i>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0"><?php echo e(__('Open Tickets')); ?></h5>
                            <span class="h2 font-weight-bold mb-0"><?php echo e($open_ticket); ?></span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                <i class="ni ni-chart-pie-35"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0"><?php echo e(__('Closed Tickets')); ?></h5>
                            <span class="h2 font-weight-bold mb-0"><?php echo e($close_ticket); ?></span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                <i class="ni ni-money-coins"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0"><?php echo e(__('Total Agents')); ?></h5>
                            <span class="h2 font-weight-bold mb-0"><?php echo e($agents); ?></span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                <i class="ni ni-chart-bar-32"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-xl-8">
            <div class="card bg-default">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="h3 text-white mb-0"><?php echo e(__('This Year Tickets')); ?></h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="chartBar" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="h3 mb-0"><?php echo e(__('Tickets by Category')); ?></h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="category_pie" class="chart-canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/dashboard/index.blade.php ENDPATH**/ ?>