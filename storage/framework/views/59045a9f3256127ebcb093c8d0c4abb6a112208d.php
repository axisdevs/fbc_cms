<?php $__env->startSection('title'); ?>
    <?php echo e(__('Ticket')); ?> - <?php echo e($ticket->ticket_id); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
    <style>


        @media (max-width: 767px) {
            .auth-layout-wrap .auth-content {
                min-width: 100%;
            }
        }

        @media (min-width: 768px) {
            .auth-layout-wrap .auth-content {
                min-width: 90%;
            }
        }

        @media (min-width: 1024px) {
            .auth-layout-wrap .auth-content {
                min-width: 50%;
            }
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="//cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('reply_description');
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('Agent_Login'); ?>
    <div style="text-align: right;float: right;width:100%;margin-right: 15px;margin-bottom: 10px; ">
        <a href="<?php echo e(route('faq')); ?>" class="btn btn-dark mt-2"><?php echo e(__('FAQ')); ?></a>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h1 class="mb-3 text-18"><?php echo e(__('Ticket')); ?> - <?php echo e($ticket->ticket_id); ?></h1>
            </div>
            <div class="card-body">
                <?php echo csrf_field(); ?>
                <div class="card">
                    <div class="card-header"><h4><?php echo e($ticket->name); ?> <small><small><?php echo e($ticket->created_at->diffForHumans()); ?></small></small></h4></div>
                    <div class="card-body">
                        <div><?php echo $ticket->description; ?></div>
<br><br>

            <div>Status : <strong><?php echo $ticket->status; ?></strong></div>
                        <?php
                            $attachments=json_decode($ticket->attachments);
                        ?>
                        <?php if(count($attachments)): ?>
                            <div class="m-1">
                                <b><?php echo e(__('Attachments')); ?> :</b>
                                <ul class="list-group list-group-flush">

                                    <?php $__currentLoopData = $attachments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $attachment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="list-group-item">
                                            <?php echo e($attachment); ?><a download="" href="<?php echo e(env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)); ?>" class="btn btn-outline-primary ml-1 p-2" title="<?php echo e(__('Download')); ?>"><i class="fa fa-download"></i></a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <br>
                <?php $__currentLoopData = $ticket->conversions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conversion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="card">
                        <div class="card-header"><h4><?php echo e($conversion->replyBy()->name); ?> <small><small><?php echo e($conversion->created_at->diffForHumans()); ?></small></small></h4></div>
                        <div class="card-body">
                            <div><?php echo $conversion->description; ?></div>
                            <?php
                                $attachments=json_decode($conversion->attachments);
                            ?>
                            <?php if(count($attachments)): ?>
                                <div class="m-1">
                                    <b><?php echo e(__('Attachments')); ?> :</b>
                                    <ul class="list-group list-group-flush">

                                        <?php $__currentLoopData = $attachments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $attachment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li class="list-group-item">
                                                <?php echo e($attachment); ?><a download="" href="<?php echo e(env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)); ?>" class="btn btn-outline-primary ml-1 p-2" title="<?php echo e(__('Download')); ?>"><i class="fa fa-download"></i></a>
                                            </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <br>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <div class="card">
                    <div class="card-body">
                        <form method="post" action="<?php echo e(route('home.reply',$ticket->ticket_id)); ?>" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <div class="form-group">
                                <label class="require"><?php echo e(__('Description')); ?></label>
                                <textarea name="reply_description" class="form-control"><?php echo e(old('reply_description')); ?></textarea>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('reply_description')); ?>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="require"><?php echo e(__('Attachments')); ?></label>
                                <div class="input-group file-group">
                                    <input type="file" multiple="" class="form-control" name="reply_attachments[]">
                                    <div class="invalid-feedback">
                                        <?php echo e($errors->first('reply_attachments')); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="mt-3 text-center">
                                <input type="hidden" name="status" value="In Progress"/>
                                <button class="btn btn-rounded btn-primary mt-2"><?php echo e(__('Update Ticket')); ?></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/show.blade.php ENDPATH**/ ?>