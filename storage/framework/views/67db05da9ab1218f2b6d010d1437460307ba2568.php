<?php $__env->startSection('title'); ?>
    <?php echo e(__('FAQ')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
    <style>
        @media (max-width: 767px) {
            .auth-layout-wrap .auth-content {
                min-width: 100%;
            }
        }

        @media (min-width: 768px) {
            .auth-layout-wrap .auth-content {
                min-width: 90%;
            }
        }

        @media (min-width: 1024px) {
            .auth-layout-wrap .auth-content {
                min-width: 50%;
            }
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('Agent_Login'); ?>
    <div style="text-align: right;float: right;width:100%;margin-right: 15px;margin-bottom: 10px; ">
        <a href="<?php echo e(route('main')); ?>" class="btn btn-dark mt-2"><?php echo e(__('Create Ticket')); ?></a>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="col-lg-5 col-md-7">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                    <h2 class="mb-3 text-18"><?php echo e(__('FAQ')); ?></h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <?php $__currentLoopData = $faqs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $faq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="card">
                                    <div class="card-header p-1 collapsed" id="headingOne" data-toggle="collapse" data-target="#collapse-<?php echo e($faq->id); ?>" aria-expanded="false" aria-controls="collapseOne">
                                        <h4 class="mb-0 py-2 pl-3 text-primary">
                                            <?php echo e($faq->title); ?>

                                        </h4>
                                    </div>

                                    <div id="collapse-<?php echo e($faq->id); ?>" class="collapse <?php if($index == 0): ?> show <?php endif; ?>" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                                        <div class="card-body">
                                            <?php echo e($faq->description); ?>

                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/fbc_cms/resources/views/faq.blade.php ENDPATH**/ ?>