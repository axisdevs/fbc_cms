<?php $__env->startSection('title'); ?>
    <?php echo e(__('Search Your Ticket')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('Agent_Login'); ?>
    <div style="text-align: right;float: right;width:100%;margin-right: 15px;margin-bottom: 10px; ">
        <a href="<?php echo e(route('faq')); ?>" class="btn btn-dark mt-2"><?php echo e(__('FAQ')); ?></a>
        <a href="<?php echo e(route('main')); ?>" class="btn btn-dark mt-2"><?php echo e(__('Create Ticket')); ?></a>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="col-lg-5 col-md-7">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                    <h2 class="mb-3 text-18"><?php echo e(__('Search Your Ticket')); ?></h2>
                </div>
                <form method="post">
                    <?php echo csrf_field(); ?>
                    <?php if(session()->has('info')): ?>
                        <div class="alert alert-primary">
                            <?php echo e(session()->get('info')); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(session()->has('status')): ?>
                        <div class="alert alert-info">
                            <?php echo e(session()->get('status')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                            </div>
                            <input id="ticket_id" class="form-control <?php echo e($errors->has('ticket_id') ? ' is-invalid' : ''); ?>" type="text" name="ticket_id" value="<?php echo e(old('ticket_id')); ?>" placeholder="<?php echo e(__('Ticket Number')); ?>" autofocus/>
                        </div>
                        <div class="invalid-feedback">
                            <?php echo e($errors->first('ticket_id')); ?>

                        </div>
                    </div>
                    <button class="btn btn-rounded btn-primary btn-block mt-2"><?php echo e(__('Search')); ?></button>

                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/search.blade.php ENDPATH**/ ?>