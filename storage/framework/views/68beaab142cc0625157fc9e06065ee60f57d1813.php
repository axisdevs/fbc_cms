<?php $__env->startSection('title'); ?>
    <?php echo e(__('Create Ticket')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/floating_chat.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="//cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
    <?php if(env('CHAT_MODULE') == 'yes'): ?>
        <script>
            var old_chat_user = getCookie('chat_user');
            $('#prime').click(function () {
                if (old_chat_user != '') {
                    // has cookie
                    $('.msg_chat').removeClass('d-none');
                    $('.msg_form').removeClass('d-block');
                    $('.msg_chat').addClass('d-block');
                    $('.msg_form').addClass('d-none');

                    getMsg();
                } else {
                    // no cookie
                    $('.msg_chat').removeClass('d-block');
                    $('.msg_form').removeClass('d-none');
                    $('.msg_chat').addClass('d-none');
                    $('.msg_form').addClass('d-block');
                }
                toggleFab();
            });

            //Toggle chat and links
            function toggleFab() {
                $('.chat').toggleClass('is-visible');
                $('.fab').toggleClass('is-visible');
                $('.chat').toggleClass('d-none');
            }

            // Email Form Submit
            $('#chat_frm_submit').on('click', function () {
                var email = $('#chat_email').val();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('chat_form.store')); ?>',
                    data: {
                        "_token": '<?php echo e(csrf_token()); ?>',
                        email: email,
                    },
                    success: function (data) {
                        if (data != 'false') {
                            setCookie('chat_user', JSON.stringify(data), 30);
                            $('.msg_chat').removeClass('d-none');
                            $('.msg_form').removeClass('d-block');
                            $('.msg_form').addClass('d-none');
                            $('.msg_chat').addClass('d-block');
                        } else if (data == 'false') {
                            $('.e_error').html('Something went wrong.!');
                        }
                    }
                });
            });
            // End Email Form Submit

            $(document).on('keyup', '#chatSend', function (e) {
                var message = $(this).val();
                if (e.keyCode == 13 && message != '') {
                    $(this).val('');

                    $.ajax({
                        type: "post",
                        url: "floating_message",
                        data: {
                            "_token": '<?php echo e(csrf_token()); ?>',
                            message: message,
                        },
                        cache: false,
                        success: function (data) {
                        },
                        error: function (jqXHR, status, err) {
                        },
                        complete: function () {
                            getMsg();
                        }
                    })
                }
            });

            // make a function to scroll down auto
            function scrollToBottomFunc() {
                $('#chat_fullscreen').animate({
                    scrollTop: $('#chat_fullscreen').get(0).scrollHeight
                }, 10);
            }

            // get Message when page is load or when msg successfully send
            function getMsg() {
                $.ajax({
                    type: "get",
                    url: "<?php echo e(route('get_message')); ?>",
                    data: "",
                    cache: false,
                    success: function (data) {
                        $('#chat_fullscreen').html(data);
                        scrollToBottomFunc();
                    }
                });
            }

            $(document).ready(function () {

                if (getCookie('chat_user') != '') {
                    var k = JSON.parse(getCookie('chat_user'));
                    var receiver_id = k.id;
                    var my_id = 0;

                    // Enable pusher logging - don't include this in production
                    Pusher.logToConsole = false;

                    var pusher = new Pusher('<?php echo e(env('PUSHER_APP_KEY')); ?>', {
                        cluster: 'ap2',
                        forceTLS: true
                    });

                    var channel = pusher.subscribe('my-channel');
                    channel.bind('my-event', function (data) {
                        /*alert(JSON.stringify(data));*/
                        if (my_id == data.from && receiver_id == data.to) {
                            getMsg();
                        }
                    });
                }

            });
        </script>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('Agent_Login'); ?>
    <div class="row w-100 pb-2">
        <div class="col-2">
            <?php if(env('CHAT_MODULE') == 'yes'): ?>
                <div>
                    <div class="fabs">
                        <div class="chat d-none">
                            <div class="chat_header">
                                <div class="chat_option">
                                    <div class="header_img">
                                        <img src="<?php echo e(asset(Storage::url('app/small.png'))); ?>"/>
                                    </div>
                                    <span id="chat_head" class="position-absolute pt-2">Agent</span>
                                </div>
                            </div>
                            <div class="msg_chat">
                                <div id="chat_fullscreen" class="chat_conversion chat_converse">
                                    <h3 class="text-center mt-5 pt-5">No Message Found.!</h3>
                                </div>
                                <div class="fab_field">
                                    <textarea id="chatSend" name="chat_message" placeholder="Send a message" class="chat_field chat_message"></textarea>
                                </div>
                            </div>
                            <div class="msg_form">
                                <div id="chat_fullscreen" class="chat_conversion chat_converse">
                                    <form class="pt-4" name="chat_form">
                                        <div class="form-group row mb-3 ml-md-2">
                                            <div class="col-sm-10 col-md-11">
                                                <div class="input-group input-group-merge input-group-alternative">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                                    </div>
                                                    <input type="text" id="chat_email" placeholder="<?php echo e(__('Enter You Email')); ?>" name="name" class="form-control" autofocus>
                                                </div>
                                                <div class="invalid-feedback d-block e_error">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4 ml-md-2">
                                            <div class="col-sm-12 col-md-7">
                                                <button class="btn btn-primary btn-sm" id="chat_frm_submit" type="button"><span><?php echo e(__('Start Chat')); ?></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a id="prime" class="fab"><i class="prime fas fa-envelope text-white"></i></a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-10 text-right">
            <a href="<?php echo e(route('faq')); ?>" class="btn btn-dark mt-2"><?php echo e(__('FAQ')); ?></a>
            <a href="<?php echo e(route('login')); ?>" class="btn btn-dark mt-2"><?php echo e(__('Agent Login')); ?></a>
            <a href="<?php echo e(route('search')); ?>" class="btn btn-dark mt-2"><?php echo e(__('Search Ticket')); ?></a>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="col-lg-12 col-md-12">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
                <a class="btn btn-primary pull-left" href="<?php echo e(url('/')); ?>">Back to Main</a>
                <div class="text-center text-muted mb-4">

                    <h1 class="mb-3 text-18"><?php echo e($sbu_name); ?> : <?php echo e(__('Create Ticket')); ?></h1>
                </div>
                <form method="post" action="<?php echo e(route('home.store',$sbu_id)); ?>" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="name" required="" value="<?php echo e(old('name')); ?>" placeholder="<?php echo e(__('Name')); ?>"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('name')); ?>

                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                </div>
                                <input class="form-control" type="email" name="email" required="" value="<?php echo e(old('email')); ?>" placeholder="<?php echo e(__('Email')); ?>"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('email')); ?>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="phone" required="" value="<?php echo e(old('phone')); ?>" placeholder="<?php echo e(__('Cell Number')); ?>"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('phone')); ?>

                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="idno" required="" value="<?php echo e(old('idno')); ?>" placeholder="<?php echo e(__('ID Number')); ?>"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('idno')); ?>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <select class="form-control select2" name="gender" required="">
                                    <option value=""><?php echo e(__('Select Gender')); ?></option>
                                        <option value="M"  <?php if(old('gender') == 'M'): ?> selected <?php endif; ?>> Male </option>
                                        <option value="F"  <?php if(old('gender') == 'F'): ?> selected <?php endif; ?>> Female </option>
                                </select>
                            </div>
                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('gender')); ?>

                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <select class="form-control select2" name="qualification" required="">
                                    <option value=""><?php echo e(__('Select Qualification')); ?></option>
                                        <option value="O-Level" <?php if(old('qualification') == 'O-Level'): ?> selected <?php endif; ?> > O-Level </option>
                                        <option value="A-Level" <?php if(old('qualification') == 'A-Level'): ?> selected <?php endif; ?>> A-Level </option>
                                        <option value="Diploma" <?php if(old('qualification') == 'Diploma'): ?> selected <?php endif; ?>> Diploma</option>
                                        <option value="Degree" <?php if(old('qualification') == 'Degree'): ?> selected <?php endif; ?>> Degree</option>
                                        <option value="Masters" <?php if(old('qualification') == 'Masters'): ?> selected <?php endif; ?>> Masters</option>
                                </select>
                            </div>
                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('qualification')); ?>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="acc_number" required="" value="<?php echo e(old('acc_number')); ?>" placeholder="<?php echo e(__('Account Number')); ?>"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('acc_number')); ?>

                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control" type="text" name="branch" required="" value="<?php echo e(old('branch')); ?>" placeholder="<?php echo e(__('Customer Branch')); ?>"/>
                            </div>
                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('branch')); ?>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <select class="form-control select2" name="category" required="">
                                    <option value=""><?php echo e(__('Select Category')); ?></option>
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($category->id); ?>" <?php if(old('category') == $category->id): ?> selected <?php endif; ?>><?php echo e($category->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <option value="0">Other</option>
                                </select>
                            </div>
                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('category')); ?>

                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-copy-04"></i></span>
                                </div>
                                <input class="form-control" type="text" name="subject" value="<?php echo e(old('subject')); ?>" placeholder="<?php echo e(__('Subject')); ?>"/>
                            </div>

                            <div class="invalid-feedback d-block">
                                <?php echo e($errors->first('subject')); ?>


                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12">

                            <textarea name="description" class="form-control" placeholder="<?php echo e(__('Description')); ?>"><?php echo e(old('description')); ?></textarea>
                            <div class="invalid-feedback  d-block">
                                <?php echo e($errors->first('description')); ?>


                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12">
                            <label class="form-control-label"><?php echo e(__('Attachments')); ?></label>
                            <div class="custom-file">
                                <input type="file" multiple="" name="attachments[]" class="custom-file-input">
                                <label class="custom-file-label" for="customFileLang"><?php echo e(__('Select file')); ?></label>
                                <div class="invalid-feedback d-block">
                                    <?php echo e($errors->first('attachments')); ?>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-3 text-center">
                        <input type="hidden" name="status" value="In Progress"/>
                        <button class="btn btn-rounded btn-primary mt-2"><?php echo e(__('Create Ticket')); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/fbc_cms/resources/views/home.blade.php ENDPATH**/ ?>