<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $__env->yieldContent('title', 'Support System'); ?> &mdash; <?php echo e(env('APP_NAME')); ?></title>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- Favicon -->
    <link rel="icon" href="<?php echo e(asset(Storage::url('app/logo.png'))); ?>" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/nucleo/css/nucleo.css')); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/select2/dist/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/bootstrap-toastr/toastr.min.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')); ?>">
    <!-- Page plugins -->

    <!-- Argon CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/argon.css?v=1.1.0')); ?>" type="text/css">

    <?php echo $__env->yieldPushContent('css'); ?>
</head>

<body>
<!-- Sidenav -->
<?php echo $__env->make('admin.partials.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!-- Main content -->
<div class="main-content" id="panel">
    <!-- Topnav -->
<?php echo $__env->make('admin.partials.topnav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!-- Header -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0"><?php echo $__env->yieldContent('title'); ?></h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <?php echo $__env->yieldContent('breadcrumb'); ?>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        <?php echo $__env->yieldContent('actions'); ?>
                    </div>
                </div>

                <!-- Card stats -->
                <?php echo $__env->yieldContent('card'); ?>
            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">
    <?php echo $__env->yieldContent('content'); ?>

    <!-- Footer -->
        <?php echo $__env->make('admin.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<!-- Argon Scripts -->


<div id="commanModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modelCommanModelLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title" id="modelCommanModelLabel"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body pt-0"></div>
        </div>
    </div>
</div>


<!-- Core -->
<script src="<?php echo e(asset('assets/vendor/jquery/dist/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/js-cookie/js.cookie.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/select2/dist/js/select2.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/bootstrap-toastr/toastr.min.js')); ?>"></script>


<script src="https://js.pusher.com/5.0/pusher.min.js"></script>

<!-- Optional JS -->
<?php echo $__env->yieldContent('scripts'); ?>

<script src="<?php echo e(asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js')); ?>"></script>

<!-- Argon JS -->
<script src="<?php echo e(asset('assets/js/argon.js?v=1.1.0')); ?>"></script>

<script src="<?php echo e(asset('assets/js/custom.js')); ?>"></script>


<?php if($message = Session::get('success')): ?>
    <script>show_msg('Success', '<?php echo $message; ?>', 'success');</script>
    <?php echo e(Session::forget('success')); ?>

<?php endif; ?>
<?php if($message = Session::get('error')): ?>
    <script>show_msg('Error', '<?php echo $message; ?>', 'error');</script>
    <?php echo e(Session::forget('error')); ?>

<?php endif; ?>

</body>
</html>
<?php /**PATH C:\xampp\htdocs\support\resources\views/layouts/admin-master.blade.php ENDPATH**/ ?>