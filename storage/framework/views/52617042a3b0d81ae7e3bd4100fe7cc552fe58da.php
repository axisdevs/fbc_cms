<?php $__env->startSection('title'); ?>
    <?php echo e(__('Create Ticket')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="//cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.tickets.index')); ?>"><?php echo e(__('Manage Ticket')); ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $__env->yieldContent('title'); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <form action="<?php echo e(route('admin.tickets.store')); ?>" method="post" enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <div class="card-header"><h4><?php echo e(__('Ticket Information')); ?></h4></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="require form-control-label"><?php echo e(__('Name')); ?></label>
                                <input class="form-control" type="text" name="name" required="">
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('name')); ?>

                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="require form-control-label"><?php echo e(__('Email')); ?></label>
                                <input class="form-control" type="email" name="email" required="">
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('email')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="require form-control-label"><?php echo e(__('Category')); ?></label>
                                <select class="form-control select2" name="category" required="">
                                    <option value=""><?php echo e(__('Select Category')); ?></option>
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('category')); ?>

                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="require form-control-label"><?php echo e(__('Status')); ?></label>
                                <select class="form-control select2" name="status" required="">
                                    <option value="In Progress"><?php echo e(__('In Progress')); ?></option>
                                    <option value="On Hold"><?php echo e(__('On Hold')); ?></option>
                                    <option value="Closed"><?php echo e(__('Closed')); ?></option>
                                </select>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('status')); ?>

                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="require form-control-label"><?php echo e(__('Subject')); ?></label>
                                <input class="form-control" type="text" name="subject" required="">
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('subject')); ?>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="require form-control-label"><?php echo e(__('Description')); ?></label>
                            <textarea name="description" class="form-control"></textarea>
                            <div class="invalid-feedback">
                                <?php echo e($errors->first('description')); ?>

                            </div>
                        </div>
                    </div>

                    <footer class="card-footer text-right">
                        <a class="btn btn-secondary mr-2" href="<?php echo e(route('admin.tickets.index')); ?>"><?php echo e(__('Cancel')); ?></a>
                        <button class="btn btn-primary" type="submit" name="save"><?php echo e(__('Submit')); ?></button>
                    </footer>
                </div>

            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-header"><h4><?php echo e(__('Attachments')); ?></h4></div>
                    <div class="card-body">
                        <div class="input-group file-group">
                            <input type="file" class="form-control" multiple="" name="attachments[]">
                            <div class="invalid-feedback">
                                <?php echo e($errors->first('attachments')); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/tickets/create.blade.php ENDPATH**/ ?>