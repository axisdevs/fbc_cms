<?php $__env->startSection('title'); ?>
    <?php echo e(__('Reply Ticket')); ?> - <?php echo e($ticket->ticket_id); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="//cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
        CKEDITOR.replace('reply_description');
        CKEDITOR.replace('note');
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.tickets.index')); ?>"><?php echo e(__('Manage Ticket')); ?></a></li>
        
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('actions'); ?>
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit-tickets')): ?>
        <button class="btn btn-sm btn-neutral" type="button" data-toggle="collapse" href="#ticket-info"><i class="fa fa-edit"></i> <?php echo e(__('Action Ticket')); ?></button>
<a class="btn btn-sm btn-danger"  href="<?php echo e(route('admin.tickets.escalate',$ticket->id)); ?>"><i class="fa fa-arrow-up"></i> <?php echo e(__('Escalate Ticket')); ?></a>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit-tickets')): ?>
        <form class="collapse" id="ticket-info" action="<?php echo e(route('admin.tickets.update',$ticket->id)); ?>" method="post" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
            <div class="row">
                <div class="col-8">
                    <div class="card">
                        <div class="card-header"><h4><?php echo e(__('Ticket Information')); ?></h4></div>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="require form-control-label"><?php echo e(__('Name')); ?></label>
                                    <input class="form-control" type="text" name="name" required="" value="<?php echo e($ticket->name); ?>">
                                    <div class="invalid-feedback">
                                        <?php echo e($errors->first('name')); ?>

                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="require form-control-label"><?php echo e(__('Email')); ?></label>
                                    <input class="form-control" type="email" name="email" required="" value="<?php echo e($ticket->email); ?>">
                                    <div class="invalid-feedback">
                                        <?php echo e($errors->first('email')); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="require form-control-label"><?php echo e(__('Category')); ?></label>
                                    <select class="form-control select2" name="category" required="">
                                        <option value=""><?php echo e(__('Select Category')); ?></option>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($category->id); ?>" <?php if($ticket->category == $category->id): ?> selected <?php endif; ?>><?php echo e($category->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <div class="invalid-feedback">
                                        <?php echo e($errors->first('category')); ?>

                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="require form-control-label"><?php echo e(__('Status')); ?></label>
                                    <select class="form-control select2" name="status" required="">
                                        <option value="In Progress" <?php if($ticket->status == 'In Progress'): ?> selected <?php endif; ?>><?php echo e(__('In Progress')); ?></option>
                                        <option value="On Hold" <?php if($ticket->status == 'On Hold'): ?> selected <?php endif; ?>><?php echo e(__('On Hold')); ?></option>
                                        <option value="Closed" <?php if($ticket->status == 'Closed'): ?> selected <?php endif; ?>><?php echo e(__('Closed')); ?></option>
                                    </select>
                                    <div class="invalid-feedback">
                                        <?php echo e($errors->first('status')); ?>

                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <label class="require form-control-label"><?php echo e(__('Subject')); ?></label>
                                    <input class="form-control" type="text" name="subject" required="" value="<?php echo e($ticket->subject); ?>">
                                    <div class="invalid-feedback">
                                        <?php echo e($errors->first('subject')); ?>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="require form-control-label"><?php echo e(__('Description')); ?></label>
                                <textarea name="description" class="form-control"><?php echo e($ticket->description); ?></textarea>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('description')); ?>

                                </div>
                            </div>
                        </div>
                        <footer class="card-footer text-right">
                            <a class="btn btn-secondary mr-2" href="<?php echo e(route('admin.tickets.index')); ?>"><?php echo e(__('Cancel')); ?></a>
                            <button class="btn btn-primary" type="submit"><?php echo e(__('Update')); ?></button>
                        </footer>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-header"><h4><?php echo e(__('Attachments')); ?></h4></div>
                        <div class="card-body">
                            <div class="input-group file-group">
                                <input type="file" multiple="" name="attachments[]" class="form-control">
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('attachments')); ?>

                                </div>
                            </div>
                            <div class="m-1">
                                <ul class="list-group list-group-flush">
                                    <?php
                                        $attachments=json_decode($ticket->attachments);
                                    ?>
                                    <?php $__currentLoopData = $attachments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $attachment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="list-group-item">
                                            <?php echo e($attachment); ?><a download="" href="<?php echo e(env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)); ?>" class="btn btn-outline-primary ml-1 p-2" title="<?php echo e(__('Download')); ?>"><i class="fa fa-download"></i></a>
                                            <a class="text-danger ml-1" title="Delete" onclick="(confirm('Are You Sure?')?(document.getElementById('user-form-<?php echo e($index); ?>').submit()):'');"><i class="nav-icon i-Close-Window font-weight-bold"></i></a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php $__currentLoopData = $attachments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $attachment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <form method="post" id="user-form-<?php echo e($index); ?>" action="<?php echo e(route('admin.tickets.attachment.destroy',[$ticket->id,$index])); ?>">
                <?php echo csrf_field(); ?>
                <?php echo method_field('DELETE'); ?>
            </form>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
    <br><br>
    <div class="row">

        <div class="col-8">

            <div class="card">
                <div class="card-header"><h4><?php echo e($ticket->name); ?> <small><small><?php echo e($ticket->created_at->diffForHumans()); ?></small></small></h4></div>
                <div class="card-body">
                    <div><?php echo $ticket->description; ?></div>
                    <?php
                        $attachments=json_decode($ticket->attachments);
                    ?>
                    <?php if(count($attachments)): ?>
                        <div class="m-1">
                            <b><?php echo e(__('Attachments')); ?> :</b>
                            <ul class="list-group list-group-flush">
                                <?php $__currentLoopData = $attachments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $attachment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="list-group-item">
                                        <?php echo e($attachment); ?><a download="" href="<?php echo e(env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)); ?>" class="btn btn-outline-primary ml-1 p-2" title="<?php echo e(__('Download')); ?>"><i class="fa fa-download"></i></a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <br>

            <?php $__currentLoopData = $ticket->conversions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $conversion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="card">
                    <div class="card-header"><h4><?php echo e($conversion->replyBy()->name); ?> <small><small><?php echo e($conversion->created_at->diffForHumans()); ?></small></small></h4></div>
                    <div class="card-body">
                        <div><?php echo $conversion->description; ?></div>
                        <?php
                            $attachments=json_decode($conversion->attachments);
                        ?>
                        <?php if(count($attachments)): ?>
                            <div class="m-1">
                                <b>Attachments :</b>
                                <ul class="list-group list-group-flush">
                                    <?php $__currentLoopData = $attachments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $attachment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li class="list-group-item">
                                            <?php echo e($attachment); ?><a download="" href="<?php echo e(env('APP_URL').Storage::url('tickets/'.$ticket->ticket_id."/".$attachment)); ?>" class="btn btn-outline-primary ml-1 p-2" title="<?php echo e(__('Download')); ?>"><i class="fa fa-download"></i></a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <br>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('reply-tickets')): ?>
            <div class="col-4">
                <div class="card">
                    <div class="card-header"><h4><?php echo e(__('Add Reply')); ?></h4></div>
                    <form method="post" action="<?php echo e(route('admin.conversion.store',$ticket->id)); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="card-body">

                            <div class="form-group">
                                <label class="require form-control-label"><?php echo e(__('Description')); ?></label>
                                <textarea name="reply_description" class="form-control"></textarea>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('reply_description')); ?>

                                </div>
                            </div>
                            <div class="form-group file-group">
                                <label class="require form-control-label"><?php echo e(__('Attachments')); ?></label>
                                <input type="file" class="form-control" multiple="" name="reply_attachments[]">
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('reply_attachments')); ?>

                                </div>
                            </div>
                        </div>

                        <footer class="card-footer text-right">
                            <button class="btn btn-primary" type="submit"><?php echo e(__('Submit')); ?></button>
                        </footer>
                    </form>
                </div>
                <div class="card mt-4">
                    <div class="card-header"><h4><?php echo e(__('Note')); ?></h4></div>
                    <form method="post" action="<?php echo e(route('admin.note.store',$ticket->id)); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="card-body">
                            <div class="form-group">
                                <textarea name="note" class="form-control"><?php echo e($ticket->note); ?></textarea>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('note')); ?>

                                </div>
                            </div>
                        </div>

                        <footer class="card-footer text-right">
                            <button class="btn btn-primary" type="submit"><?php echo e(__('Add Note')); ?></button>
                        </footer>
                    </form>
                </div>
            </div>
        <?php endif; ?>
    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/tickets/edit.blade.php ENDPATH**/ ?>