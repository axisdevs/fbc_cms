<?php $__env->startSection('title'); ?>
    <?php echo e(__('Login')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('Agent_Login'); ?>
    <div style="text-align: right;float: right;width:100%;margin-right: 15px;margin-bottom: 10px; ">
        <a href="<?php echo e(route('faq')); ?>" class="btn btn-dark mt-2"><?php echo e(__('FAQ')); ?></a>
        <a href="<?php echo e(route('main')); ?>" class="btn btn-dark mt-2"><?php echo e(__('Create Ticket')); ?></a>
        <a href="<?php echo e(route('search')); ?>" class="btn btn-dark mt-2"><?php echo e(__('Search Ticket')); ?></a>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="col-lg-5 col-md-7">
        <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                    <small>SIGN IN</small>
                </div>
                <form method="post">
                    <?php echo csrf_field(); ?>
                    <?php if(session()->has('info')): ?>
                        <div class="alert alert-primary">
                            <?php echo e(session()->get('info')); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(session()->has('status')): ?>
                        <div class="alert alert-info">
                            <?php echo e(session()->get('status')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                            </div>
                            <input id="email" class="form-control pl-2" type="email" name="email" value="<?php echo e(old('email')); ?>" placeholder="Enter your email" autofocus/>
                        </div>
                        <div class="invalid-feedback d-block">
                            <?php echo e($errors->first('email')); ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                            </div>
                            <input id="password" class="form-control pl-2" type="password" name="password" placeholder="Enter your password"/>
                        </div>
                        <div class="invalid-feedback">
                            <?php echo e($errors->first('password')); ?>

                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-primary mt-2"><?php echo e(__('Sign In')); ?></button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <div class="text-center">
                    <a href="<?php echo e(route('password.request')); ?>" class="text-light"><small>Forgot password?</small></a>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/auth/login.blade.php ENDPATH**/ ?>