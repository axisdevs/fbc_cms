<?php $__env->startSection('title'); ?>
    <?php echo e(__('Report By Channel')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.users')); ?>"><?php echo e(__('Report')); ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $__env->yieldContent('title'); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h4><?php echo e(__('Report By Channel')); ?></h4></div>
                <div class="card-body">
                    <br><br>
                    <form method="post" class="needs-validation" action="<?php echo e(route('admin.report.check')); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>



                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Channel')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control select2" name="channel" required>
                                   <option>Please select channel</option>
                                   <option value='Web'>CMS Site - Web</option>
                                   <option value="Twitter">From Twitter</option>
                                   <option value="Whatsapp">From Whatsapp</option>
                                   <option value="Facebook">From Facebook</option>
                                </select>
                            </div>
                        </div>




                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button class="btn btn-primary"><span><?php echo e(__('Generate')); ?></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/fbc_cms/resources/views/admin/report/create.blade.php ENDPATH**/ ?>