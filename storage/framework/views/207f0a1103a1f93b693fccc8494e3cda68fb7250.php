<?php $__env->startSection('title'); ?>
    <?php echo e(__('Create Faq')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.faq')); ?>"><?php echo e(__('Manage Faq')); ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $__env->yieldContent('title'); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h4><?php echo e(__('Add a New Faq')); ?></h4></div>
                <div class="card-body">
                    <br><br>
                    <form method="post" class="needs-validation" action="<?php echo e(route('admin.faq.store')); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Title')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" placeholder="<?php echo e(__('Title of the Faq')); ?>" name="title" class="form-control <?php echo e($errors->has('title') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('title')); ?>" autofocus>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('title')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Description')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <textarea name="description" class="form-control <?php echo e($errors->has('description') ? ' is-invalid' : ''); ?>"><?php echo e(old('description')); ?></textarea>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('description')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button class="btn btn-primary"><span><?php echo e(__('Add')); ?></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/faq/create.blade.php ENDPATH**/ ?>