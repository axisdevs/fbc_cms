<?php $__env->startSection('title'); ?>
    <?php echo e(__('Create User')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.users')); ?>"><?php echo e(__('Manage Users')); ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $__env->yieldContent('title'); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h4><?php echo e(__('Add a New User')); ?></h4></div>
                <div class="card-body">
                    <br><br>
                    <form method="post" class="needs-validation" action="<?php echo e(route('admin.users.store')); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Name')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" placeholder="<?php echo e(__('Full name of the user')); ?>" name="name" class="form-control <?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('name')); ?>" autofocus>
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('name')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Email')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <input type="email" placeholder="<?php echo e(__('Email address (should be unique)')); ?>" name="email" class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" value="<?php echo e(old('email')); ?>">
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('email')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Role')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control select2" name="role">
                                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Business Unit')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control select2" name="sbu_id" required>
                                    <?php $__currentLoopData = $sbus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sbu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($sbu->id); ?>"><?php echo e($sbu->sbu_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Escalation Group')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control select2" name="escalation_levels" required>
                                    <?php $__currentLoopData = $escalation_groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $esc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($esc->Level); ?>"><?php echo e($esc->Description); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-4 <?php echo e($errors->has('is_desgnated_point_person') ? ' has-error ' : ''); ?>">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="is_desgnated_point_person"><?php echo e(__('Is Designated Point Person')); ?></label>
                            <div class="col-sm-12 col-md-7 pt-2">
                                <label class="custom-toggle">
                                    <input type="checkbox" name="is_desgnated_point_person" id="is_desgnated_point_person" >
                                    <span class="custom-toggle-slider rounded-circle" data-label-off="Off" data-label-on="On"></span>
                                </label>
                                <?php if($errors->has('is_desgnated_point_person')): ?>
                                    <span class="error-block">
                                        <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                        <?php echo e($errors->first('is_desgnated_point_person')); ?>

                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Password')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <input type="password" name="password" autocomplete="new-password" placeholder="<?php echo e(__('Set an account password')); ?>" class="form-control <?php echo e($errors->has('password') ? ' is-invalid': ''); ?>">
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('password')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Confirm Password')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <input type="password" name="password_confirmation" placeholder="<?php echo e(__('Confirm account password')); ?>" autocomplete="new-password" class="form-control <?php echo e($errors->has('password_confirmation') ? ' is-invalid': ''); ?>">
                                <div class="invalid-feedback">
                                    <?php echo e($errors->first('password_confirmation')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Avatar')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <input class="form-control <?php if ($errors->has('avatar')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('avatar'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="avatar" type="file" id="avatar">
                                <span class="invalid-feedback">
                                    <?php echo e($errors->first('avatar')); ?>

                                </span>
                                <span>
                                    <small><?php echo e(__('Please upload a valid image file. Size of image should not be more than 2MB.')); ?></small>
                                </span>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button class="btn btn-primary"><span><?php echo e(__('Add')); ?></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/users/create.blade.php ENDPATH**/ ?>