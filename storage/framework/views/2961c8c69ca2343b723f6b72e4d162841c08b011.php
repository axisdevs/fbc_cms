<?php $__env->startSection('title'); ?>
    <?php echo e(__('Site Settings ')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h4><?php echo e(__('Update Site Settings')); ?></h4></div>
                <div class="card-body">
                    <br><br>
                    <form method="post" action="<?php echo e(route('admin.setting')); ?>" enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"><?php echo e(__('Site Logo')); ?></label>
                            <div class="col-sm-12 col-md-7">
                                <div class="row">
                                    <div class="col-sm-8 col-md-10">
                                        <input class="form-control <?php if ($errors->has('avatar')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('avatar'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="logo" type="file" id="logo">
                                        <span class="invalid-feedback">
                                            <?php echo e($errors->first('logo')); ?>

                                        </span>
                                        <span>
                                            <small><?php echo e(__('Please upload a valid png image file. Size of image should not be more than 1MB.')); ?></small>
                                        </span>
                                    </div>
                                    <div class="col-sm-4 col-md-2">
                                        <img src="<?php echo e(asset(Storage::url('app/logo.png'))); ?>" alt="logo" class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="form-group row mb-4 <?php echo e($errors->has('mail_driver') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_driver">
                                    <?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_driver_label')); ?>

                                </label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_driver" id="mail_driver" class="form-control" value="<?php echo e(env('MAIL_DRIVER')); ?>" placeholder="<?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_driver_placeholder')); ?>"/>
                                    <?php if($errors->has('mail_driver')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('mail_driver')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('mail_host') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_host"><?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_host_label')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_host" id="mail_host" class="form-control" value="<?php echo e(env('MAIL_HOST')); ?>" placeholder="<?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_host_placeholder')); ?>"/>
                                    <?php if($errors->has('mail_host')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('mail_host')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('mail_port') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_port"><?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_port_label')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="number" name="mail_port" id="mail_port" class="form-control" value="<?php echo e(env('MAIL_PORT')); ?>" placeholder="<?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_port_placeholder')); ?>"/>
                                    <?php if($errors->has('mail_port')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('mail_port')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('mail_username') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_username"><?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_username_label')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_username" id="mail_username" class="form-control" value="<?php echo e(env('MAIL_USERNAME')); ?>" placeholder="<?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_username_placeholder')); ?>"/>
                                    <?php if($errors->has('mail_username')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('mail_username')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('mail_password') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_password"><?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_password_label')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_password" id="mail_password" class="form-control" value="<?php echo e(env('MAIL_PASSWORD')); ?>" placeholder="<?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_password_placeholder')); ?>"/>
                                    <?php if($errors->has('mail_password')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('mail_password')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('mail_encryption') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="mail_encryption"><?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_encryption_label')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="mail_encryption" id="mail_encryption" class="form-control" value="<?php echo e(env('MAIL_ENCRYPTION')); ?>" placeholder="<?php echo e(trans('installer_messages.environment.wizard.form.app_tabs.mail_encryption_placeholder')); ?>"/>
                                    <?php if($errors->has('mail_encryption')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('mail_encryption')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('lang') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="lang"><?php echo e(__('Default Front Language')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <select name="lang" id="lang" class="form-control select2">
                                        <?php $__currentLoopData = $lang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php if(env('CUR_LANG') == $row): ?> selected <?php endif; ?>><?php echo e($row); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if($errors->has('lang')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('lang')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('chat') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="chat"><?php echo e(__('Chat Module')); ?></label>
                                <div class="col-sm-12 col-md-7 pt-2">
                                    <label class="custom-toggle">
                                        <input type="checkbox" name="chat" id="chat" <?php if(env('CHAT_MODULE') == 'yes'): ?> checked <?php endif; ?>>
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="Off" data-label-on="On"></span>
                                    </label>
                                    <?php if($errors->has('chat')): ?>
                                        <span class="error-block">
                                            <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                            <?php echo e($errors->first('chat')); ?>

                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            
                            <div class="form-group row mb-4 <?php echo e($errors->has('pusher_app_id') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="pusher_app_id"><?php echo e(__('Pusher App Id')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="pusher_app_id" id="pusher_app_id" class="form-control" value="<?php echo e(env('PUSHER_APP_ID')); ?>" placeholder="<?php echo e(__('Pusher App Id')); ?>"/>
                                    <?php if($errors->has('pusher_app_id')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('pusher_app_id')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('pusher_app_key') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="pusher_app_key"><?php echo e(__('Pusher App Key')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="pusher_app_key" id="pusher_app_key" class="form-control" value="<?php echo e(env('PUSHER_APP_KEY')); ?>" placeholder="<?php echo e(__('Pusher App Key')); ?>"/>
                                    <?php if($errors->has('pusher_app_key')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('pusher_app_key')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('pusher_app_secret') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="pusher_app_secret"><?php echo e(__('Pusher App Secret')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="pusher_app_secret" id="pusher_app_secret" class="form-control" value="<?php echo e(env('PUSHER_APP_SECRET')); ?>" placeholder="<?php echo e(__('Pusher App Secret')); ?>"/>
                                    <?php if($errors->has('pusher_app_secret')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('pusher_app_secret')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row mb-4 <?php echo e($errors->has('pusher_app_cluster') ? ' has-error ' : ''); ?>">
                                <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3" for="pusher_app_cluster"><?php echo e(__('Pusher App Cluster')); ?></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" name="pusher_app_cluster" id="pusher_app_cluster" class="form-control" value="<?php echo e(env('PUSHER_APP_CLUSTER')); ?>" placeholder="<?php echo e(__('Pusher App Cluster')); ?>"/>
                                    <?php if($errors->has('pusher_app_cluster')): ?>
                                        <span class="error-block">
                                    <i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i>
                                    <?php echo e($errors->first('pusher_app_cluster')); ?>

                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label form-control-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button class="btn btn-primary"><span><?php echo e(__('Save')); ?></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/users/setting.blade.php ENDPATH**/ ?>