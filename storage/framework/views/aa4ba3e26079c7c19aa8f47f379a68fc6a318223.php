<?php $__env->startSection('title'); ?>
    <?php echo e(__('Choose SBU')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/floating_chat.css')); ?>">

    <style>
        .addReadMore.showlesscontent .SecSec,
        .addReadMore.showlesscontent .readLess {
            display: none;
        }

        .addReadMore.showmorecontent .readMore {
            display: none;
        }

        .addReadMore .readMore,
        .addReadMore .readLess {
            font-weight: bold;
            margin-left: 2px;
            color: blue;
            cursor: pointer;
        }

        .addReadMoreWrapTxt.showmorecontent .SecSec,
        .addReadMoreWrapTxt.showmorecontent .readLess {
            display: block;
        }
        </style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    
    

<script>
    function AddReadMore() {
        //This limit you can set after how much characters you want to show Read More.
        var carLmt = 50;
        // Text to show when text is collapsed
        var readMoreTxt = " ... Read More";
        // Text to show when text is expanded
        var readLessTxt = " Read Less";


        //Traverse all selectors with this class and manupulate HTML part to show Read More
        $(".addReadMore").each(function() {
            if ($(this).find(".firstSec").length)
                return;

            var allstr = $(this).text();
            // alert(allstr);
            if (allstr.length > carLmt) {
                var firstSet = allstr.substring(0, carLmt);
                var secdHalf = allstr.substring(carLmt, allstr.length);

                var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span><a class='readMore'  title='Click to Show More'>" + readMoreTxt + "</a><span class='readLess' title='Click to Show Less'>" + readLessTxt + "</span>";
                $(this).html(strtoadd);
            }

        });
        //Read More and Read Less Click Event binding
        $(document).on("click", ".readMore,.readLess", function() {
            $(this).closest(".addReadMore").toggleClass("showlesscontent showmorecontent");
        });
    }

    $(function() {
        //Calling function after Page Load
        AddReadMore();
    });
</script>

    <?php if(env('CHAT_MODULE') == 'yes'): ?>
        <script>
            var old_chat_user = getCookie('chat_user');
            $('#prime').click(function () {
                if (old_chat_user != '') {
                    // has cookie
                    $('.msg_chat').removeClass('d-none');
                    $('.msg_form').removeClass('d-block');
                    $('.msg_chat').addClass('d-block');
                    $('.msg_form').addClass('d-none');

                    getMsg();
                } else {
                    // no cookie
                    $('.msg_chat').removeClass('d-block');
                    $('.msg_form').removeClass('d-none');
                    $('.msg_chat').addClass('d-none');
                    $('.msg_form').addClass('d-block');
                }
                toggleFab();
            });

            //Toggle chat and links
            function toggleFab() {
                $('.chat').toggleClass('is-visible');
                $('.fab').toggleClass('is-visible');
                $('.chat').toggleClass('d-none');
            }

            // Email Form Submit
            $('#chat_frm_submit').on('click', function () {
                var email = $('#chat_email').val();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('chat_form.store')); ?>',
                    data: {
                        "_token": '<?php echo e(csrf_token()); ?>',
                        email: email,
                    },
                    success: function (data) {
                        if (data != 'false') {
                            setCookie('chat_user', JSON.stringify(data), 30);
                            $('.msg_chat').removeClass('d-none');
                            $('.msg_form').removeClass('d-block');
                            $('.msg_form').addClass('d-none');
                            $('.msg_chat').addClass('d-block');
                        } else if (data == 'false') {
                            $('.e_error').html('Something went wrong.!');
                        }
                    }
                });
            });
            // End Email Form Submit

            $(document).on('keyup', '#chatSend', function (e) {
                var message = $(this).val();
                if (e.keyCode == 13 && message != '') {
                    $(this).val('');

                    $.ajax({
                        type: "post",
                        url: "floating_message",
                        data: {
                            "_token": '<?php echo e(csrf_token()); ?>',
                            message: message,
                        },
                        cache: false,
                        success: function (data) {
                        },
                        error: function (jqXHR, status, err) {
                        },
                        complete: function () {
                            getMsg();
                        }
                    })
                }
            });

            // make a function to scroll down auto
            function scrollToBottomFunc() {
                $('#chat_fullscreen').animate({
                    scrollTop: $('#chat_fullscreen').get(0).scrollHeight
                }, 10);
            }

            // get Message when page is load or when msg successfully send
            function getMsg() {
                $.ajax({
                    type: "get",
                    url: "<?php echo e(route('get_message')); ?>",
                    data: "",
                    cache: false,
                    success: function (data) {
                        $('#chat_fullscreen').html(data);
                        scrollToBottomFunc();
                    }
                });
            }

            $(document).ready(function () {

                if (getCookie('chat_user') != '') {
                    var k = JSON.parse(getCookie('chat_user'));
                    var receiver_id = k.id;
                    var my_id = 0;

                    // Enable pusher logging - don't include this in production
                    Pusher.logToConsole = false;

                    var pusher = new Pusher('<?php echo e(env('PUSHER_APP_KEY')); ?>', {
                        cluster: 'ap2',
                        forceTLS: true
                    });

                    var channel = pusher.subscribe('my-channel');
                    channel.bind('my-event', function (data) {
                        /*alert(JSON.stringify(data));*/
                        if (my_id == data.from && receiver_id == data.to) {
                            getMsg();
                        }
                    });
                }

            });
        </script>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('Agent_Login'); ?>
    <div class="row w-100 pb-2">
        <div class="col-2">
            <?php if(env('CHAT_MODULE') == 'yes'): ?>
                <div>
                    <div class="fabs">
                        <div class="chat d-none">
                            <div class="chat_header">
                                <div class="chat_option">
                                    <div class="header_img">
                                        <img src="<?php echo e(asset(Storage::url('app/small.png'))); ?>"/>
                                    </div>
                                    <span id="chat_head" class="position-absolute pt-2">Agent</span>
                                </div>
                            </div>
                            <div class="msg_chat">
                                <div id="chat_fullscreen" class="chat_conversion chat_converse">
                                    <h3 class="text-center mt-5 pt-5">No Message Found.!</h3>
                                </div>
                                <div class="fab_field">
                                    <textarea id="chatSend" name="chat_message" placeholder="Send a message" class="chat_field chat_message"></textarea>
                                </div>
                            </div>
                            <div class="msg_form">
                                <div id="chat_fullscreen" class="chat_conversion chat_converse">
                                    <form class="pt-4" name="chat_form">
                                        <div class="form-group row mb-3 ml-md-2">
                                            <div class="col-sm-10 col-md-11">
                                                <div class="input-group input-group-merge input-group-alternative">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                                    </div>
                                                    <input type="text" id="chat_email" placeholder="<?php echo e(__('Enter You Email')); ?>" name="name" class="form-control" autofocus>
                                                </div>
                                                <div class="invalid-feedback d-block e_error">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4 ml-md-2">
                                            <div class="col-sm-12 col-md-7">
                                                <button class="btn btn-primary btn-sm" id="chat_frm_submit" type="button"><span><?php echo e(__('Start Chat')); ?></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a id="prime" class="fab"><i class="prime fas fa-envelope text-white"></i></a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-10 text-right">
            <a href="<?php echo e(route('faq')); ?>" class="btn btn-dark mt-2"><?php echo e(__('FAQ')); ?></a>
            <a href="<?php echo e(route('login')); ?>" class="btn btn-dark mt-2"><?php echo e(__('Agent Login')); ?></a>
            <a href="<?php echo e(url('search')); ?>" class="btn btn-dark mt-2"><?php echo e(__('Search Ticket')); ?></a>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">

        <!-- Card -->
        <?php $__currentLoopData = $sbus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sbu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="card promoting-card col-md-4 col-sm-4">

      <!-- Card content -->
      <div class="card-body d-flex flex-row">

        <!-- Avatar -->
        <img src="<?php echo e(asset('assets/img/avatar/fbc.png')); ?>" class="rounded-circle mr-3" height="50px" width="50px" alt="avatar">

        <!-- Content -->
        <div>

          <!-- Title -->
        <h4 class="card-title font-weight-bold mb-2"><?php echo e($sbu->sbu_name); ?></h4>
          <!-- Subtitle -->
          

        </div>

      </div>

      <!-- Card image -->
      <div class="view overlay">
        <img class="card-img-top rounded-0" src="<?php echo e(asset('assets/img/avatar/fbc_main.png')); ?>" alt="FBC Main Image">
      <a href="<?php echo e(url('/sbu/sbu-ref='.$sbu->id.'/home')); ?>">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!-- Card content -->
      <div class="card-body">

        <div class="collapse-content">

          <!-- Text -->
        <p class="addReadMore showlesscontent" id=""><?php echo e($sbu->sbu_description); ?></p>
          <!-- Button -->
          <a class="btn btn-flat red-text p-1 my-1 mr-0 mml-1 collapsed" data-toggle="collapse" href="#collapseContent" aria-expanded="false" aria-controls="collapseContent"></a>
          <i class="fas fa-share-alt text-muted float-right p-1 my-1" data-toggle="tooltip" data-placement="top" title="Share this post"></i>
          <i class="fas fa-heart text-muted float-right p-1 my-1 mr-3" data-toggle="tooltip" data-placement="top" title="I like it"></i>

        </div>

      </div>

    </div>
    <!-- Card -->
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

      </div>








  </div>
  <!-- Card package -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/SBUS.blade.php ENDPATH**/ ?>