<footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-12">
            <div class="copyright text-center text-lg-right text-muted">
                &copy; 2019 <a href="#" class="font-weight-bold ml-1"><?php echo e(env('APP_NAME')); ?></a> <?php echo e(__('All rights reserved')); ?>

            </div>
        </div>
    </div>
</footer>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/fbc_cms/resources/views/admin/partials/footer.blade.php ENDPATH**/ ?>