<?php $__env->startSection('title'); ?>
    <?php echo e(__('Business Units')); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.dashboard')); ?>"><i class="fas fa-home"></i> <?php echo e(__('Dashboard')); ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $__env->yieldContent('title'); ?></li>
    </ol>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('actions'); ?>
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create-users')): ?>
        <a href="<?php echo e(route('admin.users.create')); ?>" class="btn btn-sm btn-neutral"><?php echo e(__('Add')); ?> <i class="fa fa-user-plus"></i></a>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card o-hidden mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable-basic" class="table dataTable-collapse text-center">
                            <thead class="thead-light">
                            <tr>
                                
                                <th scope="col"><?php echo e(__('Action')); ?></th>
                                <th scope="col"><?php echo e(__('Business Unit Name')); ?></th>
                                <th scope="col"><?php echo e(__('Description')); ?></th>
                                <th scope="col"><?php echo e(__('Max Required Exec Time')); ?></th>
                                <th scope="col"><?php echo e(__('Created Date')); ?></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $buss_units; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $buss_unit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    
                                    <td>
                                        
                                        <a class="text-success mr-4" title="<?php echo e(__('Edit')); ?>" href=""><i class="fa fa-pen font-weight-bold"></i></a>
                                    </td>
                                    
                                    <td class="text-left"><?php echo e($buss_unit->sbu_name); ?></td>
                                    <td class="text-left"><?php echo e($buss_unit->sbu_description); ?></td>
                                    <td class="text-left"><?php echo e($buss_unit->max_allowed_resolution_time); ?></td>
                                    <td><span class="badge badge-primary"><?php echo e(date('d M y',strtotime($buss_unit->max_allowed_resolution_time))); ?></span></td>


                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\support\resources\views/admin/buss_units/index.blade.php ENDPATH**/ ?>