<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="">
                <img src="<?php echo e(asset(Storage::url('app/logo.png'))); ?>" class="navbar-brand-img" alt="">
            </a>
            <div class="ml-auto">
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link <?php echo e(request()->is('*dashboard*') ? ' active' : ''); ?>" href="<?php echo e(route('admin.dashboard')); ?>">
                            <i class="ni ni-shop text-primary"></i>
                            <span class="nav-link-text"><?php echo e(__('Dashboard')); ?></span>
                        </a>
                    </li>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('manage-users')): ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo e(request()->is('*users*') ? ' active' : ''); ?>" href="<?php echo e(route('admin.users')); ?>">
                                <i class="ni ni-single-02 text-info"></i>
                                <span class="nav-link-text"><?php echo e(__('Users')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo e(request()->is('*ticket*') ? ' active' : ''); ?>" href="#navbar-maps" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                            <i class="ni ni-briefcase-24 text-dark"></i>
                            <span class="nav-link-text"><?php echo e(__('Ticket')); ?></span>
                        </a>
                        <div class="collapse <?php echo e(request()->is('*ticket*') ? ' show' : ''); ?>" id="navbar-maps">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item <?php echo e(request()->is('*ticket') ? ' active' : ''); ?>">
                                    <a href="<?php echo e(route('admin.tickets.index')); ?>" class="nav-link">
                                        <?php echo e(__('All')); ?>

                                    </a>
                                </li>
                                <li class="nav-item <?php echo e(request()->is('*ticket/in-progress') ? ' active' : ''); ?>">
                                    <a href="<?php echo e(route('admin.tickets.index','in-progress')); ?>" class="nav-link">
                                        <span class="item-name"><?php echo e(__('In Progress')); ?></span>
                                    </a>
                                </li>
                                <li class="nav-item <?php echo e(request()->is('*ticket/on-hold') ? ' active' : ''); ?>">
                                    <a href="<?php echo e(route('admin.tickets.index','on-hold')); ?>" class="nav-link">
                                        <span class="item-name"><?php echo e(__('On Hold')); ?></span>
                                    </a>
                                </li>
                                <li class="nav-item <?php echo e(request()->is('*ticket/closed') ? ' active' : ''); ?>">
                                    <a href="<?php echo e(route('admin.tickets.index','closed')); ?>" class="nav-link">
                                        <span class="item-name"><?php echo e(__('Closed')); ?></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('manage-category')): ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo e(request()->is('*category*') ? ' active' : ''); ?>" href="<?php echo e(route('admin.category')); ?>">
                                <i class="ni ni-archive-2 text-green"></i>
                                <span class="nav-link-text"><?php echo e(__('Category')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('manage-faq')): ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo e(request()->is('*faq*') ? ' active' : ''); ?>" href="<?php echo e(route('admin.faq')); ?>">
                                <i class="ni ni-ungroup text-orange"></i>
                                <span class="nav-link-text"><?php echo e(__('FAQ')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if(env('CHAT_MODULE') == 'yes'): ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo e(request()->is('*chat*') ? ' active' : ''); ?>" href="<?php echo e(route('admin.chats')); ?>">
                                <i class="ni ni-chat-round text-primary"></i>
                                <span class="nav-link-text"><?php echo e(__('Chat')); ?></span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('manage-setting')): ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo e(request()->is('*setting*') ? ' active' : ''); ?>" href="<?php echo e(route('admin.setting')); ?>">
                                <i class="ni ni-settings text-red"></i>
                                <span class="nav-link-text"><?php echo e(__('Setting')); ?></span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link <?php echo e(request()->is('*business_units*') ? ' active' : ''); ?>" href="<?php echo e(route('admin.buss_units')); ?>">
                                <i class="ni ni-books text-success"></i>
                                <span class="nav-link-text"><?php echo e(__('Business Units')); ?></span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link <?php echo e(request()->is('*channel-report*') ? ' active' : ''); ?>" href="#navbar-maps" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                                <i class="ni ni-chart-bar-32 text-dark"></i>
                                <span class="nav-link-text"><?php echo e(__('Reports')); ?></span>
                            </a>
                            <div class="collapse <?php echo e(request()->is('*channel-report*') ? ' show' : ''); ?>" id="navbar-maps">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item <?php echo e(request()->is('*channel-report') ? ' active' : ''); ?>">
                                        <a href="<?php echo e(route('admin.report.channel')); ?>" class="nav-link">
                                            <?php echo e(__('By Channel')); ?>

                                        </a>
                                    </li>
                                    <li class="nav-item <?php echo e(request()->is('*reports/category') ? ' active' : ''); ?>">
                                        <a href="<?php echo e(route('admin.report.category')); ?>" class="nav-link">
                                            <span class="item-name"><?php echo e(__('By Category')); ?></span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php echo e(request()->is('*reports/sentiment') ? ' active' : ''); ?>">
                                        <a href="<?php echo e(route('admin.tickets.index','on-hold')); ?>" class="nav-link">
                                            <span class="item-name"><?php echo e(__('Sentiment Analysis')); ?></span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</nav>
<?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/fbc_cms/resources/views/admin/partials/sidebar.blade.php ENDPATH**/ ?>