-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2020 at 11:47 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sts`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `color`, `created_at`, `updated_at`) VALUES
(1, 'Account Opening', '#800000', '2020-01-29 09:02:15', '2020-01-29 09:02:15'),
(2, 'System Related', '#0000a0', '2020-02-14 07:50:07', '2020-02-14 07:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `conversions`
--

CREATE TABLE `conversions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachments` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conversions`
--

INSERT INTO `conversions` (`id`, `ticket_id`, `description`, `attachments`, `sender`, `created_at`, `updated_at`) VALUES
(1, 27, '<p>hi</p>', '[]', 'user', '2020-01-31 08:16:15', '2020-01-31 08:16:15'),
(2, 34, '<p>Please find attached the summary of ticket requirements</p>', '[\"FBC Test.docx\"]', '1', '2020-02-14 04:50:10', '2020-02-14 04:50:10'),
(3, 34, '<p>Noted&nbsp;</p>', '[\"FBC Test.docx\"]', 'user', '2020-02-14 04:51:30', '2020-02-14 04:51:30'),
(4, 35, '<p>Provide More details</p>', '[]', '7', '2020-02-14 07:22:45', '2020-02-14 07:22:45'),
(5, 35, '<p>Find attached my summary</p>', '[\"FBC Test.docx\"]', 'user', '2020-02-14 07:28:48', '2020-02-14 07:28:48');

-- --------------------------------------------------------

--
-- Table structure for table `escalation_groups`
--

CREATE TABLE `escalation_groups` (
  `id` int(11) NOT NULL,
  `Level` int(11) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `escalation_groups`
--

INSERT INTO `escalation_groups` (`id`, `Level`, `Description`, `created_at`) VALUES
(1, 1, 'First Line Support', '2020-02-03 12:56:57'),
(2, 2, 'Middle-Level Support', '2020-02-03 12:56:57'),
(3, 3, 'SBU Management Level', '2020-02-03 12:56:57'),
(4, 4, 'Group Executive Level', '2020-02-03 12:56:57');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'How to handle account opening process', 'This entails covering account request to account closure. All this should be done from incepton to when the customer is satisfied', '2020-01-29 09:05:19', '2020-01-29 09:05:19'),
(2, 'Account not running', 'Please call 0773629282', '2020-02-04 03:46:07', '2020-02-04 03:46:07'),
(3, 'Hello', 'Hello World State', '2020-02-04 03:46:23', '2020-02-04 03:46:23'),
(4, 'Issues On Account Opening', 'Please note that our system is up and running. Please contact 04 0000 000 1', '2020-02-14 07:15:58', '2020-02-14 07:15:58');

-- --------------------------------------------------------

--
-- Table structure for table `floating_chat_messages`
--

CREATE TABLE `floating_chat_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from` bigint(20) NOT NULL,
  `to` bigint(20) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `floating_chat_messages`
--

INSERT INTO `floating_chat_messages` (`id`, `from`, `to`, `message`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'Hi', 1, '2020-01-30 03:44:47', '2020-02-14 06:32:28'),
(2, 0, 1, 'How are you', 0, '2020-02-04 03:41:17', '2020-02-04 03:41:17'),
(3, 2, 0, 'hi Checking status', 1, '2020-02-04 03:42:26', '2020-02-04 03:44:05'),
(4, 0, 2, 'How are you', 0, '2020-02-04 03:42:45', '2020-02-04 03:42:45'),
(5, 0, 2, 'Zvirisei', 0, '2020-02-04 03:43:24', '2020-02-04 03:43:24');

-- --------------------------------------------------------

--
-- Table structure for table `floating_chat_users`
--

CREATE TABLE `floating_chat_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_end` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `floating_chat_users`
--

INSERT INTO `floating_chat_users` (`id`, `email`, `is_end`, `created_at`, `updated_at`) VALUES
(1, 'takundamadzamba@gmail.com', 0, '2020-01-30 03:44:43', '2020-01-30 03:44:43'),
(2, 'takundamadzamba@gmail.com', 0, '2020-02-04 03:42:16', '2020-02-04 03:42:16');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from` bigint(20) NOT NULL,
  `to` bigint(20) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_read` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_13_122456_create_notifications_table', 1),
(4, '2019_04_13_124848_create_permission_tables', 1),
(5, '2019_04_13_165922_add_avatar_field_to_users_table', 1),
(6, '2019_06_29_071130_create_tickets_table', 1),
(7, '2019_07_01_043311_create_category_table', 1),
(8, '2019_07_02_043628_create_conversion_table', 1),
(9, '2019_08_14_112204_add_note_to_tickets_table', 1),
(10, '2019_10_14_100826_create_faqs_table', 1),
(11, '2019_12_25_042118_create_messages_table', 1),
(12, '2019_12_30_043939_create_floating_chat_users_table', 1),
(13, '2019_12_30_043950_create_floating_chat_messages_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(1, 'App\\User', 4),
(1, 'App\\User', 8),
(2, 'App\\User', 2),
(2, 'App\\User', 3),
(2, 'App\\User', 5),
(2, 'App\\User', 6),
(2, 'App\\User', 7);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'manage-users', 'web', '2020-01-29 08:32:49', '2020-01-29 08:32:49'),
(2, 'view-users', 'web', '2020-01-29 08:32:50', '2020-01-29 08:32:50'),
(3, 'create-users', 'web', '2020-01-29 08:32:50', '2020-01-29 08:32:50'),
(4, 'edit-users', 'web', '2020-01-29 08:32:50', '2020-01-29 08:32:50'),
(5, 'delete-users', 'web', '2020-01-29 08:32:50', '2020-01-29 08:32:50'),
(6, 'lang-manage', 'web', '2020-01-29 08:32:50', '2020-01-29 08:32:50'),
(7, 'lang-change', 'web', '2020-01-29 08:32:50', '2020-01-29 08:32:50'),
(8, 'manage-tickets', 'web', '2020-01-29 08:32:51', '2020-01-29 08:32:51'),
(9, 'create-tickets', 'web', '2020-01-29 08:32:51', '2020-01-29 08:32:51'),
(10, 'edit-tickets', 'web', '2020-01-29 08:32:51', '2020-01-29 08:32:51'),
(11, 'delete-tickets', 'web', '2020-01-29 08:32:51', '2020-01-29 08:32:51'),
(12, 'manage-category', 'web', '2020-01-29 08:32:51', '2020-01-29 08:32:51'),
(13, 'create-category', 'web', '2020-01-29 08:32:51', '2020-01-29 08:32:51'),
(14, 'edit-category', 'web', '2020-01-29 08:32:51', '2020-01-29 08:32:51'),
(15, 'delete-category', 'web', '2020-01-29 08:32:51', '2020-01-29 08:32:51'),
(16, 'reply-tickets', 'web', '2020-01-29 08:32:52', '2020-01-29 08:32:52'),
(17, 'manage-setting', 'web', '2020-01-29 08:32:52', '2020-01-29 08:32:52'),
(18, 'manage-faq', 'web', '2020-01-29 08:32:52', '2020-01-29 08:32:52'),
(19, 'create-faq', 'web', '2020-01-29 08:32:52', '2020-01-29 08:32:52'),
(20, 'edit-faq', 'web', '2020-01-29 08:32:52', '2020-01-29 08:32:52'),
(21, 'delete-faq', 'web', '2020-01-29 08:32:52', '2020-01-29 08:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2020-01-29 08:32:48', '2020-01-29 08:32:48'),
(2, 'Agent', 'web', '2020-01-29 08:32:53', '2020-01-29 08:32:53');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(2, 2),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(16, 2),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sbus`
--

CREATE TABLE `sbus` (
  `id` int(11) NOT NULL,
  `sbu_name` varchar(30) NOT NULL,
  `sbu_description` varchar(250) DEFAULT NULL,
  `max_allowed_resolution_time` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sbus`
--

INSERT INTO `sbus` (`id`, `sbu_name`, `sbu_description`, `max_allowed_resolution_time`, `date_created`) VALUES
(1, 'FBC BANK', 'All Issues on cards, bancassurance, agency banking, investment banking, treasury services, investmen', '72 hours', '2020-01-29 12:54:14'),
(2, 'FBC Building Society', 'Inquire on all mortgage-related issues. Access mortage with all satisfacroty benefits and bank with FBC ', '24 hours', '2020-01-29 12:54:14'),
(5, 'FBC Insurances', 'Inquire on motor vehicles, house and all forms of insurances. Use FBC insurance SBU to inquire. The ', '72 hours', '2020-01-29 13:00:17'),
(6, 'FBC Securities', 'Inquire on expert financial trends and the Zimbabwe stock exchange indices.', '24 hours', '2020-01-29 13:00:17'),
(12, 'FBC Reinsurance', 'This includes inquiries on Cyber risk and advocacy for insurance against data breaches, risk solutio', '48 hours', '2020-01-29 13:06:27'),
(13, 'FBC Microplan', 'Affordable micro-plans including school loans and order financing for business boosts. Micro plan do', '24 hours', '2020-01-29 13:06:27'),
(14, 'FBC - Other ', 'This classifies inquiry on non-business units information. If you have any other FBC group related inquiry please post it under this tag', '16 hours', '2020-01-29 13:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sbu_id` int(11) DEFAULT NULL,
  `ticket_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idno` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` int(11) NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachments` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_escalation_level` int(11) NOT NULL DEFAULT 1 COMMENT 'Escalation Level ',
  `issue_escalated_to` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `must_close_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `sbu_id`, `ticket_id`, `name`, `email`, `phone`, `idno`, `gender`, `acc_number`, `branch`, `qualification`, `category`, `subject`, `status`, `description`, `attachments`, `note`, `issue_escalation_level`, `issue_escalated_to`, `created_at`, `updated_at`, `must_close_at`) VALUES
(33, 1, '1581661710', 'Takunda', 'pmupfurutsa@axissol.com', '0773629282', '2310101301923', 'M', '092772121', 'Test Samora', 'O-Level', 1, 'Hi', 'Closed', '<p>Hi I need a new acount</p>', '[]', '<p><strong>This ticket will be resolved for a moment once don</strong>e</p><p>&nbsp;</p>', 1, 7, '2020-02-14 04:28:30', '2020-02-14 04:41:15', '2020-02-17 06:28:30'),
(34, 1, '1581662683', 'Takunda', 'takundamadzamba@gmail.com', '0773629282', '03-00912912-120', 'M', '033-0989-0823', 'Nkwame', 'Diploma', 1, 'Prospective Offers', 'Closed', '<p>Hi I need to have a new account, what should i do</p>', '[]', NULL, 1, 7, '2020-02-14 04:44:43', '2020-02-14 04:52:31', '2020-02-17 06:44:43'),
(35, 1, '1581668456', 'Takunda', 'tmadzamba@axissol.com', '0773629282', '2310101301923', 'M', '100-89127-9923', 'Nkwame', 'Masters', 1, 'Checking my account details', 'Closed', '<p><strong>Hi CAN I have the USSD CODE TO USE</strong></p><ul><li><strong>1 For test etc</strong></li><li><strong>Etc</strong></li></ul>', '[\"FBC Test.docx\"]', NULL, 1, 7, '2020-02-14 06:20:56', '2020-02-14 07:27:30', '2020-02-17 08:20:56'),
(36, 1, '1581668683', 'Test User', 'tmadzamba@axissol.com', '0773629282', '09-099912992', 'M', '0988-0821312312', 'Nelson Mandela', 'A-Level', 1, 'Test Request', 'In Progress', '<p>Hi This is a test request</p>', '[\"FBC Test.docx\"]', NULL, 1, 7, '2020-02-14 06:24:43', '2020-02-14 06:24:43', '2020-02-17 08:24:43'),
(37, 1, '1581669330', 'Takunda', 'developers@axissol.com', '0773629282', '2323', 'M', '900-8772-652', 'Nkwame Branch', 'A-Level', 1, 'Test request for nkwame branch', 'In Progress', '<p><strong>Hi </strong></p><p>&nbsp;</p><ul><li><em><strong>This is a Test request</strong></em></li></ul>', '[\"FBC Test.docx\"]', NULL, 1, 7, '2020-02-14 06:35:30', '2020-02-14 06:35:30', '2020-02-17 08:35:30'),
(38, 1, '1581669800', 'Hello', 'tmadzamba@axissol.com', '0773629282', '2310101301923', 'M', '900-009', 'nkwame', 'Diploma', 1, 'Account Opening', 'In Progress', '<p>New acc</p>', '[]', NULL, 1, 7, '2020-02-14 06:43:20', '2020-02-14 06:43:20', '2020-02-17 08:43:20'),
(39, 1, '1581670436', 'Test User', 'tmadzamba@axissol.com', '0773629282', '0987654567', 'M', '100-987-5624', 'Nkwame', 'Diploma', 1, 'Account Opening', 'In Progress', '<p>Test</p>', '[]', NULL, 1, 7, '2020-02-14 06:53:56', '2020-02-14 06:53:56', '2020-02-17 08:53:56'),
(40, 1, '1581671332', 'Test Account', 'tmadzamba@axissol.com', '0773629282', '1082130129', 'M', '09912121', 'Nkwame Branch', 'A-Level', 1, 'Test Requst', 'In Progress', '<p>Hi</p><p>&nbsp;</p><p><em><strong>This is a test request -&nbsp; Demo</strong></em></p>', '[\"FBC Test.docx\"]', NULL, 1, 7, '2020-02-14 07:08:52', '2020-02-14 07:08:52', '2020-02-17 09:08:52');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_logs`
--

CREATE TABLE `ticket_logs` (
  `id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `activity_type` varchar(30) NOT NULL,
  `old_value` varchar(50) NOT NULL,
  `new_value` varchar(50) NOT NULL,
  `comment` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `sbu_id` int(11) NOT NULL,
  `escalation_levels` int(11) NOT NULL,
  `is_desgnated_point_person` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `avatar`, `parent`, `lang`, `sbu_id`, `escalation_levels`, `is_desgnated_point_person`) VALUES
(1, 'Admin', 'admin@fbc.co.zw', NULL, '$2y$10$0z2ctI5tzhc3Xu5Fd0hiHugLbOJzHBCPcZByytk1l8Hrtjdn12SKa', NULL, '2020-01-29 08:32:52', '2020-01-29 08:32:52', NULL, 0, 'en', 1, 0, 0),
(2, 'Agent', 'agent@fbc.co.zw', NULL, '$2y$10$1gteVjbiYsX6N.8ZnSXoXOuyAEIL7GInRMjiOQ.663EPdxwtxnwnG', NULL, '2020-01-29 08:32:53', '2020-01-29 08:32:53', NULL, 1, 'en', 3, 1, 1),
(5, 'Taku Madzamba', 'takundamadzamba@gmail.com', NULL, '$2y$10$krJB5up6R3CZaKJTV.JeZen.souHWF1wyACy4IbTaz3lPeaVYKZT6', NULL, '2020-02-04 07:06:39', '2020-02-04 07:06:39', NULL, 1, 'en', 5, 1, 1),
(6, 'Mathe', 'mathe@fbc.co.zw', NULL, '$2y$10$Vgntn/fiVwfAiy2WaICNIOsBSLYiLfekLTM8KEFQ1KqfQm7gybDrK', NULL, '2020-02-04 07:27:00', '2020-02-04 07:27:00', NULL, 1, 'en', 1, 3, 1),
(7, 'First Line Support', 'fbc@fbc.co.zw', NULL, '$2y$10$vhN/IrPlUMzfMUIvi4kTfu0WvfrWDUygY/XHXK9hgau3nnIKfTXHC', NULL, '2020-02-04 07:30:13', '2020-02-04 07:30:13', NULL, 1, 'en', 1, 1, 0),
(8, 'Baker', 'baker@fbc.co.zw', NULL, '$2y$10$.scEVciC1evlJh/NS6Iwme6x.fLZUPI/sNqFzfUEEW43x/ulNdxvS', NULL, '2020-02-04 07:33:50', '2020-02-04 07:33:50', NULL, 1, 'en', 2, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversions`
--
ALTER TABLE `conversions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `escalation_groups`
--
ALTER TABLE `escalation_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Level` (`Level`),
  ADD UNIQUE KEY `Description` (`Description`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floating_chat_messages`
--
ALTER TABLE `floating_chat_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floating_chat_users`
--
ALTER TABLE `floating_chat_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sbus`
--
ALTER TABLE `sbus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sbu_name` (`sbu_name`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tickets_ticket_id_unique` (`ticket_id`);

--
-- Indexes for table `ticket_logs`
--
ALTER TABLE `ticket_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `conversions`
--
ALTER TABLE `conversions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `escalation_groups`
--
ALTER TABLE `escalation_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `floating_chat_messages`
--
ALTER TABLE `floating_chat_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `floating_chat_users`
--
ALTER TABLE `floating_chat_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sbus`
--
ALTER TABLE `sbus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `ticket_logs`
--
ALTER TABLE `ticket_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
