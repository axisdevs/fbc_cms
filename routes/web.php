<?php
Route::get('/', 'HomeController@index')->name('main');

Route::get('ticket/{id}', ['as' => 'home.view','uses' =>'HomeController@view']);
Route::post('ticket/{id}', ['as' => 'home.reply','uses' =>'HomeController@reply']);

Route::get('faq', ['as' => 'faq','uses' =>'HomeController@faq']);
Route::get('search', ['as' => 'search','uses' =>'HomeController@search']);
Route::post('search', ['as' => 'ticket.search','uses' =>'HomeController@ticketSearch']);


Route::name('admin.')->prefix('admin')->middleware('auth')->group(function() {
    Route::get('dashboard', 'DashboardController')->name('dashboard');

    Route::get('users/roles', 'UserController@roles')->name('users.roles');
    Route::resource('users', 'UserController', [
        'names' => [
            'index' => 'users'
        ]
    ]);
    Route::get('lang/clear',['as' => 'lang.clear','uses' =>'LanguageController@clear']);

    Route::get('lang/create',['as' => 'lang.create','uses' =>'LanguageController@create']);
    Route::post('lang/create',['as' => 'lang.store','uses' =>'LanguageController@store']);
    Route::get('lang/{lang}',['as' => 'lang.index','uses' =>'LanguageController@index']);
    Route::post('lang/{lang}',['as' => 'lang.store.data','uses' =>'LanguageController@storeData']);

    Route::get('lang/change/{lang}',['as' => 'lang.update','uses' =>'LanguageController@update']);

    Route::get('ticket/create',['as' => 'tickets.create','uses' =>'TicketController@create']);
    Route::post('ticket',['as' => 'tickets.store','uses' =>'TicketController@store']);
    Route::get('ticket/{status?}',['as' => 'tickets.index','uses' =>'TicketController@index']);
    Route::get('ticket/{id}/edit',['as' => 'tickets.edit','uses' =>'TicketController@editTicket']); //escalate
    Route::get('ticket/{id}/escalate',['as' => 'tickets.escalate','uses' =>'TicketController@escalateTicket']); //escalate
    Route::delete('ticket/{id}/destroy',['as' => 'tickets.destroy','uses' =>'TicketController@destroy']);
    Route::delete('ticket-attachment/{tid}/destroy/{id}',['as' => 'tickets.attachment.destroy','uses' =>'TicketController@attachmentDestroy']);
    Route::put('ticket/{id}/update',['as' => 'tickets.update','uses' =>'TicketController@updateTicket']);

    Route::get('category/create',['as' => 'category.create','uses' =>'CategoryController@create']);
    Route::post('category',['as' => 'category.store','uses' =>'CategoryController@store']);
    Route::get('category',['as' => 'category','uses' =>'CategoryController@index']);
    Route::get('category/{id}/edit',['as' => 'category.edit','uses' =>'CategoryController@edit']);
    Route::delete('category/{id}/destroy',['as' => 'category.destroy','uses' =>'CategoryController@destroy']);
    Route::put('category/{id}/update',['as' => 'category.update','uses' =>'CategoryController@update']);

    Route::get('faq/create',['as' => 'faq.create','uses' =>'FaqController@create']);
    Route::post('faq',['as' => 'faq.store','uses' =>'FaqController@store']);
    Route::get('faq',['as' => 'faq','uses' =>'FaqController@index']);
    Route::get('faq/{id}/edit',['as' => 'faq.edit','uses' =>'FaqController@edit']);
    Route::delete('faq/{id}/destroy',['as' => 'faq.destroy','uses' =>'FaqController@destroy']);
    Route::put('faq/{id}/update',['as' => 'faq.update','uses' =>'FaqController@update']);

//    Route::('category',['as' => 'category','uses' =>'CategoryController@index']);
    Route::post('ticket/{id}/conversion',['as' => 'conversion.store','uses' =>'ConversionController@store']);
    Route::post('ticket/{id}/note',['as' => 'note.store','uses' =>'TicketController@storeNote']);

    Route::get('setting',['as' => 'setting','uses' =>'UserController@setting']);
    Route::post('setting',['as' => 'setting.store','uses' =>'UserController@save_setting']);

    Route::get('business_units',['as' => 'buss_units','uses' =>'bussUnitsController@buss_units']);

    Route::post('ticket/{id}/update_escalation',['as' => 'escalation.update','uses' =>'TicketController@update_escalation']);

    Route::get('channel-report',['as' => 'report.channel','uses' =>'ReportController@channelShow']);
    Route::get('category-report',['as' => 'report.category','uses' =>'ReportController@categoryShow']);

    Route::post('channel-check',['as' => 'report.check','uses' =>'ReportController@checkChannel']);
    Route::post('category-check',['as' => 'report.catcheck','uses' =>'ReportController@checkCategory']);
    // Message Route
    Route::get('chat',['as' => 'chats','uses' =>'MessageController@index']);
    Route::get('message/{id}',['as' => 'message','uses' => 'MessageController@getMessage']);
    Route::post('message', 'MessageController@sendMessage');
    // End Message Route

});

Route::get('home', ['as' => 'home','uses' =>'HomeController@index']);

Route::prefix('sbu/sbu-ref={sbu_id}/')->group(function () {

    Route::get('home', ['as' => 'home','uses' =>'HomeController@new_ticket_index']);
    Route::post('home', ['as' => 'home.store','uses' =>'HomeController@store']);
    });

Route::get('get_message',['as' => 'get_message','uses' => 'MessageController@getFloatingMessage']);
Route::post('message_form',['as' => 'chat_form.store','uses' =>'MessageController@store']);
Route::post('floating_message', 'MessageController@sendFloatingMessage');

Route::middleware('auth')->get('logout', function() {
    Auth::logout();
    return redirect(route('login'))->withInfo('You have successfully logged out!');
})->name('logout');

Auth::routes(['verify' => true]);

