<?php

namespace App\Traits;

use Auth;
use Storage;

trait UserTrait
{
    public function getProfilelinkAttribute()
    {
        return route('admin.users.edit', ['user' => $this->id]);
    }

    public function getAvatarlinkAttribute()
    {
        if(Storage::disk('public')->exists($this->avatar))
        {
            return Storage::disk('public')->url("/app/public/" . $this->avatar);
        }

        $image_id = substr($this->id, -1, 1);
        $image_id = $image_id % 5;
        if($image_id == 0)
        {
            $image_id = 5;
        }

        return asset('assets/img/avatar/avatar-' . $image_id . '.png');
    }

    public function getIsmeAttribute()
    {
        if(Auth::check() && Auth::id() == $this->id)
        {
            return true;
        }

        return false;
    }
}
