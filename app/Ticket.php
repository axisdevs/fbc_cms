<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'ticket_id','name', 'email', 'category','subject','status','description','attachments','note','sbu_id','phone','idno','gender','acc_number','branch',
        'qualification','issue_escalation_level','issue_escalated_to','must_close_at','channel'
    ];

    public function conversions(){
        return $this->hasMany('App\Conversion','ticket_id','id')->orderBy('id');
    }

    public function ticket_assignee(){
        return $this->hasOne('App\User', 'id', 'issue_escalated_to');
    }

    public function ticket_level(){
        return $this->hasOne('App\EscalationGroup', 'id', 'issue_escalation_level');
    }




}
