<?php

namespace App\Http\Controllers;

use App\Category;
use App\Ticket;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    //

    public function channelShow(){
        // $channels = Ticket::groupBy('channel');
        return view('admin.report.create');
    }

    public function categoryShow(){
         $categories = Category::all();
        return view('admin.report.category.create',[
            'categories' => $categories
        ]);
    }



    public function checkChannel(Request $request){
       $all_channel_tickets = Ticket::where('channel',$request->input('channel'))->get();
       $channel = 'Report by Channel: '.$request->input('channel');
       return view('admin.report.index')->with('tickets',$all_channel_tickets)
                                        ->with('channel',$channel);
    }

    public function checkCategory(Request $request){
        $all_channel_tickets = Ticket::where('category',$request->input('category'))->get();
        $channel = 'Report by  Category: '.$request->input('category');
        return view('admin.report.category.index')->with('tickets',$all_channel_tickets)
                                         ->with('category',$channel);
     }
}
