<?php

namespace App\Http\Controllers;

use App\Category;
use App\Conversion;
use App\Faq;
use App\Mail\SendTicket;
use App\Mail\SendTicketAdmin;
use App\Mail\SendTicketReply;
use App\Sbu;
use App\Ticket;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    public function __construct()
    {
        \App::setLocale(env('CUR_LANG'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        if(!file_exists(storage_path() . "/installed"))
        {
            return redirect('install');
        }
        if(Auth::user())
        {
            return redirect()->route('admin.dashboard');
        }

        $sbus = Sbu::orderBy('id','Asc')->get();


        //        dd($_COOKIE['chat_user']);
        return view('SBUS', compact('sbus'));
    }

    public function new_ticket_index($sbu_id)
    {


        // if(!file_exists(storage_path() . "/installed"))
        // {
        //     return redirect('install');
        // }
        // if(Auth::user())
        // {
        //     return redirect()->route('admin.dashboard');
        // }

        $categories = Category::all();
        $sbus = Sbu::findOrFail($sbu_id);
        $sbu_name = $sbus->sbu_name;


        //        dd($_COOKIE['chat_user']);
        return view('home', compact('categories','sbu_name','sbu_id'));
    }

    public function search()
    {
        return view('search');
    }

    public function faq()
    {
        $faqs = Faq::get();

        return view('faq', compact('faqs'));
    }

    public function ticketSearch(Request $request)
    {
        $validation = [
            'ticket_id' => ['required'],
        ];
        $this->validate($request, $validation);
        $ticket = Ticket::where('ticket_id', '=', $request->ticket_id)->first();

        if($ticket)
        {
            return redirect()->route('home.view', \Illuminate\Support\Facades\Crypt::encrypt($ticket->ticket_id));

        }
        else
        {
            return redirect()->back()->with('info', __('Invalid Ticket Number'));
        }

        return view('search');
    }

    public function store($sbu_id,Request $request)
    {

        // dd($request->all());
        $validation = [
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
            ],
            'category' => [
                'required',
                'string',
                'max:255',
            ],
            'subject' => [
                'required',
                'string',
                'max:255',
            ],
            'phone' => [
                'required',
                'string',
                'max:255',
            ],
            'acc_number' => [
                'required',
                'string',
                'max:255',
            ],
            'status' => [
                'required',
                'string',
                'max:100',
            ],
            'description' => ['required'],
        ];

        $direct_user = User::where('sbu_id',$sbu_id)
                    ->where('escalation_levels',1)
                    ->where('is_desgnated_point_person',1)
                    ->get();



        if(count($direct_user) == 0){

            $direct_user = User::where('sbu_id',$sbu_id)
                                ->where('escalation_levels','>',0)
                                ->orderBy('escalation_levels','Asc')
                                ->get();
                }


        if(count($direct_user) > 0){
            $esc_level = $direct_user[0]->escalation_levels;
            $esc_user =  $direct_user[0]->id;
        }
        else{
            $esc_level = 0;
            $esc_user = 0;
        }

        // dd($esc_level." == ".$esc_user);


        if($request->hasfile('attachments'))
        {
            $validation['attachments.*'] = 'mimes:doc,pdf,docx,zip,png,jpeg,jpg';
        }
        $this->validate($request, $validation);

        $sbu_info = Sbu::find($sbu_id);
        $sbu_max_time = $sbu_info->max_allowed_resolution_time;

        $post                = $request->all();
        $post['ticket_id'] = time();
        $post['sbu_id'] = $sbu_id;
        $post["issue_escalation_level"] = $esc_level;
        $post["issue_escalated_to"] = $esc_user;
        $post['must_close_at'] = date("Y-m-d H:i:s", strtotime('+'.$sbu_max_time));
        $post['channel'] = 'Web';

        $data              = [];
        if($request->hasfile('attachments'))
        {
            foreach($request->file('attachments') as $file)
            {
                $name = $file->getClientOriginalName();
                $file->move(storage_path() . '/tickets/' . $post['ticket_id'] . "/", $name);
                $data[] = $name;
            }
        }
        $post['attachments'] = json_encode($data);
        $ticket              = Ticket::create($post);

        // Send Email to User
        try
        {
            Mail::to($ticket->email)->send(new SendTicket($ticket));

           // $users = User::join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')->where('model_has_roles.model_type', '=', 'App\User')->where('role_id', '=', 1)->get();

            $users = $direct_user;

            foreach($users as $user)
            {
                Mail::to($user->email)
                        ->cc(config('app.CONTINGENCY_MAIL'))
                        ->send(new SendTicketAdmin($user, $ticket));
            }
        }
        catch(\Exception $e)
        {
            $error_msg = "E-Mail has been not sent due to SMTP configuration, error: ".$e->getMessage();
        }

        return redirect()->back()->with('success', __('Ticket created successfully') . ' <a href="' . route('home.view', \Illuminate\Support\Facades\Crypt::encrypt($ticket->ticket_id)) . '"><b>' . __('Your unique ticket link is this.') . '</b></a> ' . ((isset($error_msg)) ? '<br> <span class="text-danger">' . $error_msg . '</span>' : ''));
    }

    public function view($ticket_id)
    {
        $ticket_id = \Illuminate\Support\Facades\Crypt::decrypt($ticket_id);
        $ticket    = Ticket::where('ticket_id', '=', $ticket_id)->first();
        if($ticket)
        {
            return view('show', compact('ticket'));
        }
        else
        {
            return redirect()->back()->with('error', __('Ticket not found.'));
        }
    }

    public function reply(Request $request, $ticket_id)
    {
        $ticket = Ticket::where('ticket_id', '=', $ticket_id)->first();
        if($ticket)
        {
            $validation = ['reply_description' => ['required']];
            if($request->hasfile('reply_attachments'))
            {
                $validation['reply_attachments.*'] = 'mimes:doc,pdf,docx,zip,png,jpeg,jpg';
            }
            $this->validate($request, $validation);

            $post                = [];
            $post['sender']      = 'user';
            $post['ticket_id']   = $ticket->id;
            $post['description'] = $request->reply_description;

            $data                = [];
            if($request->hasfile('reply_attachments'))
            {
                foreach($request->file('reply_attachments') as $file)
                {
                    $name = $file->getClientOriginalName();
                    $file->move(storage_path() . '/tickets/' . $ticket->ticket_id . "/", $name);
                    $data[] = $name;
                }
            }
            $post['attachments'] = json_encode($data);
            $conversion          = Conversion::create($post);

            // Send Email to User
            try
            {
                $users = User::join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')->where('model_has_roles.model_type', '=', 'App\User')->where('role_id', '=', 1)->get();
                foreach($users as $user)
                {
                    Mail::to($user->email)->send(new SendTicketReply($user, $ticket, $conversion));
                }
            }
            catch(\Exception $e)
            {
                $error_msg = "E-Mail has been not sent due to SMTP configuration ";
            }

            return redirect()->back()->with('success', __('Reply added successfully') . ((isset($error_msg)) ? '<br> <span class="text-danger">' . $error_msg . '</span>' : ''));
        }
        else
        {
            return redirect()->back()->with('error', __('Some thing is wrong'));
        }
    }
}
