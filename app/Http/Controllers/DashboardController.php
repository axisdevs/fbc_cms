<?php

namespace App\Http\Controllers;

use App\Category;
use App\Sbu;
use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __invoke(Request $request)
    {
        $categories   = Category::count();
        $open_ticket  = Ticket::whereIn(
            'status', [
                        'On Hold',
                        'In Progress',
                    ]
        )->count();
        $close_ticket = Ticket::where('status', '=', 'Closed')->count();
        $agents       = \DB::table('model_has_roles')->where('model_type', '=', 'App\User')->where('role_id', '=', '2')->count();

        $categoriesChart = Ticket::select(
            [
                'categories.name',
                'categories.color',
                \DB::raw('count(*) as total'),
            ]
        )->join('categories', 'categories.id', '=', 'tickets.category')->groupBy('categories.id')->get();

        $chartData            = [];
        $chartData['data']    = [];
        $chartData['color']   = [];
        $chartData['name'][]  = [];
        $chartData['value'][] = [];

        if(count($categoriesChart) > 0)
        {
            foreach($categoriesChart as $category)
            {
                /*$chartData['data'][]  = [
                    'value' => $category->total,
                    'name' => $category->name,
                ];*/
                $chartData['name'][]  = $category->name;
                $chartData['value'][] = $category->total;
                $chartData['color'][] = $category->color;
            }
        }

        $monthData = [];
        $barChart  = Ticket::select(
            [
                \DB::raw('MONTH(created_at) as month'),
                \DB::raw('YEAR(created_at) as year'),
                \DB::raw('count(*) as total'),
            ]
        )->where('created_at', '>', \DB::raw('DATE_SUB(NOW(),INTERVAL 1 YEAR)'))->groupBy(
            [
                \DB::raw('MONTH(created_at)'),
                \DB::raw('YEAR(created_at)'),
            ]
        )->get();

        $YearDate = strtotime(date("Y-m-d", time()) . " -1 year");
        for($i = 1; $i <= 12; $i++)
        {

            $YearDate = strtotime(date("Y-m-d", $YearDate) . " +1 month");

            $monthData[date('M', $YearDate)] = 0;
            foreach($barChart as $chart)
            {
                if(intval($chart->month) == intval(date('m', $YearDate)))
                {
                    $monthData[date('M', $YearDate)] = $chart->total;
                }
            }
        }

        $buss_units = Auth::user()->sbu_id;

        $sbu = Sbu::find($buss_units);
        $sbu_name  = $sbu->sbu_name;


        return view('admin.dashboard.index', compact('categories', 'open_ticket', 'close_ticket', 'agents', 'chartData', 'monthData','sbu_name'));
    }
}
