<?php

namespace App\Http\Controllers;

use App\Sbu;
use Illuminate\Http\Request;

class bussUnitsController extends Controller
{
    //

    public function buss_units(){
        $buss_units = Sbu::all();
        return view('admin.buss_units.index',[
            'buss_units' => $buss_units
        ]);
    }
}
