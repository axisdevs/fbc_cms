<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\SendTicket;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = '')
    {
        $user = \Auth::user();
        if($user->can('manage-tickets'))
        {
            $tickets = Ticket::select(
                [
                    'tickets.*',
                    'categories.name as category_name',
                    'categories.color',
                ]
            )->join('categories', 'categories.id', '=', 'tickets.category');
            if($status == 'in-progress')
            {
                $tickets->where('status', '=', 'In Progress');
            }
            elseif($status == 'on-hold')
            {
                $tickets->where('status', '=', 'On Hold');
            }
            elseif($status == 'closed')
            {
                $tickets->where('status', '=', 'Closed');
            }
            $tickets = $tickets->get();

            return view('admin.tickets.index', compact('tickets'));
        }
        else
        {
            return view('403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = \Auth::user();
        if($user->can('create-tickets'))
        {
            $categories = Category::get();

            return view('admin.tickets.create', compact('categories'));
        }
        else
        {
            return view('403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = \Auth::user();
        if($user->can('create-tickets'))
        {

            $validation = [
                'name' => [
                    'required',
                    'string',
                    'max:255',
                ],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                ],
                'category' => [
                    'required',
                    'string',
                    'max:255',
                ],
                'subject' => [
                    'required',
                    'string',
                    'max:255',
                ],
                'status' => [
                    'required',
                    'string',
                    'max:100',
                ],
                'description' => ['required'],
            ];
            if($request->hasfile('attachments'))
            {
                $validation['attachments.*'] = 'mimes:doc,pdf,docx,zip,png,jpeg,jpg';
            }
            $this->validate($request, $validation);

            $post              = $request->all();
            $post['ticket_id'] = time();
            $post['channel'] = 'Web';

            $data              = [];
            if($request->hasfile('attachments'))
            {
                foreach($request->file('attachments') as $file)
                {
                    $name = $file->getClientOriginalName();
                    $file->move(storage_path() . '/tickets/' . $post['ticket_id'] . "/", $name);
                    $data[] = $name;
                }
            }
            $post['attachments'] = json_encode($data);
            $ticket              = Ticket::create($post);


            // Send Email to User
            try
            {
                Mail::to($ticket->email)->send(new SendTicket($ticket));
            }
            catch(\Exception $e)
            {
                $error_msg = "E-Mail has been not sent due to SMTP configuration ";
            }

            // Send Email to
            if(isset($error_msg))
            {
                Session::put('smtp_error', '<span class="text-danger">' . $error_msg . '</span>');
            }
            Session::put('ticket_id', ' <a class="text text-white" href="' . route('home.view', \Illuminate\Support\Facades\Crypt::encrypt($ticket->ticket_id)) . '"><b>' . __('Your unique ticket link is this.') . '</b></a><br><br> ');

            // return redirect()->back()->with('success', __('Ticket created successfully') . ' <a href="' . route('home.view', \Illuminate\Support\Facades\Crypt::encrypt($ticket->ticket_id)) . '"><b>' . __('Your unique ticket link is this.') . '</b></a> ' . ((isset($error_msg)) ? '<br> <span class="text-danger">' . $error_msg . '</span>' : ''));
            return redirect()->route('admin.tickets.index')->with('success', __('Ticket created successfully'));
        }
        else
        {
            return view('403');
        }
    }

    public function storeNote($ticketID, Request $request)
    {
        $user = \Auth::user();
        if($user->can('reply-tickets'))
        {
            $validation = [
                'note' => ['required'],
            ];
            $this->validate($request, $validation);

            $ticket = Ticket::find($ticketID);
            if($ticket)
            {
                $ticket->note = $request->note;
                $ticket->save();

                return redirect()->back()->with('success', __('Ticket note saved successfully'));
            }
            else
            {
                return view('403');
            }
        }
        else
        {
            return view('403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Ticket $ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Ticket $ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function editTicket($id)
    {
        $user = \Auth::user();
        if($user->can('edit-tickets'))
        {
            $ticket = Ticket::find($id);
            if($ticket)
            {
                $categories = Category::get();

                return view('admin.tickets.edit', compact('ticket', 'categories'));
            }
            else
            {
                return view('403');
            }
        }
        else
        {
            return view('403');
        }
    }

    public function escalateTicket($id)
    {
        $user = \Auth::user();
        if($user->can('edit-tickets'))
        {
            $ticket = Ticket::find($id);
            $ticket_sbu = $ticket->sbu_id;
            $curr_esc_lev = $ticket->issue_escalation_level;
            if($ticket)
            {
                $categories = Category::get();
                $escalate_resource = User::where('sbu_id',$ticket_sbu)
                                            ->where('escalation_levels','>',$curr_esc_lev)
                                            ->get();
                return view('admin.tickets.escalate', compact('ticket', 'categories','escalate_resource'));
            }
            else
            {
                return view('403');
            }
        }
        else
        {
            return view('403');
        }
    }

    public function update_escalation(Request $request,$ticket_id){
        $user = \Auth::user();

        $validation = [
            'issue_escalated_to' => [
                'required',
                'integer',
                'max:255',
            ]
        ];

        $this->validate($request, $validation);
                $post = $request->all();


                $user = User::find($post['issue_escalated_to']);
                $esc_level = $user->escalation_levels;
                $name = $user->name;
                $ticket = Ticket::find($ticket_id);
                $post['issue_escalation_level'] = $esc_level;

                $ticket->update($post);

                return redirect()->back()->with('success', __('Ticket has been successfully escalated to '.$name));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Ticket $ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function updateTicket(Request $request, $id)
    {
        $user = \Auth::user();
        if($user->can('edit-tickets'))
        {
            $ticket = Ticket::find($id);
            if($ticket)
            {
                $validation = [
                    'name' => [
                        'required',
                        'string',
                        'max:255',
                    ],
                    'email' => [
                        'required',
                        'string',
                        'email',
                        'max:255',
                    ],
                    'category' => [
                        'required',
                        'string',
                        'max:255',
                    ],
                    'subject' => [
                        'required',
                        'string',
                        'max:255',
                    ],
                    'status' => [
                        'required',
                        'string',
                        'max:100',
                    ],
                    'description' => ['required'],
                ];
                if($request->hasfile('attachments'))
                {
                    $validation['attachments.*'] = 'mimes:doc,pdf,docx,zip,png,jpeg,jpg';
                }
                $this->validate($request, $validation);

                $post = $request->all();
                if($request->hasfile('attachments'))
                {
                    $data = json_decode($ticket->attachments, true);
                    foreach($request->file('attachments') as $file)
                    {
                        $name = $file->getClientOriginalName();
                        $file->move(storage_path() . '/tickets/' . $ticket->ticket_id . "/", $name);
                        $data[] = $name;
                    }
                    $post['attachments'] = json_encode($data);
                }
                $ticket->update($post);

                return redirect()->back()->with('success', __('Ticket updated successfully'));
            }
            else
            {
                return view('403');
            }
        }
        else
        {
            return view('403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Ticket $ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = \Auth::user();
        if($user->can('edit-tickets'))
        {
            $ticket = Ticket::find($id);
            $ticket->delete();

            return redirect()->back()->with('success', __('Ticket deleted successfully'));
        }
        else
        {
            return view('403');
        }
    }

    public function attachmentDestroy($ticket_id, $id)
    {
        $user = \Auth::user();
        if($user->can('edit-tickets'))
        {
            $ticket      = Ticket::find($ticket_id);
            $attachments = json_decode($ticket->attachments);
            if(isset($attachments[$id]))
            {
                Storage::delete(storage_path() . '/tickets/' . $ticket->ticket_id . "/" . $attachments[$id]);
                unset($attachments[$id]);
                $ticket->attachments = json_encode(array_values($attachments));
                $ticket->save();

                return redirect()->back()->with('success', __('Attachment deleted successfully'));
            }
            else
            {
                return redirect()->back()->with('error', __('Attachment is missing'));
            }
        }
        else
        {
            return view('403');
        }
    }

    public function sendSMS(){

    }
}
