<?php

namespace App\Http\Controllers;

use App;
use App\EscalationGroup;
use App\Http\Requests\
{UserAddRequest};
use App\Sbu;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = \Auth::user();

        if($user->can('manage-users'))
        {

            $this->authorize(User::class, 'index');
            $users = User::where('parent', '=', \Auth::user()->getCreatedBy())->get();

            return view('admin.users.index', compact('users'));
        }
        else
        {
            return view('403');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = \Auth::user();
        if($user->can('create-users'))
        {
            $roles = Role::get();
            $escalation_groups = EscalationGroup::get();
            $sbus = Sbu::all();
            return view('admin.users.create', compact('sbus','roles','escalation_groups'));
        }
        else
        {
            return view('403');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserAddRequest $request)
    {
        $user = \Auth::user();
        if($user->can('create-users'))
        {



            if($request->is_desgnated_point_person == "on"){
                $boolean_role = true;
            }
            else{
                $boolean_role = false;
            }
            //check if there is default escalation point at each level
            $verify_designated_point = User::where('sbu_id',$request->sbu_id)
                                            ->where('escalation_levels',$request->escalation_levels)
                                            ->where('is_desgnated_point_person',$boolean_role)
                                            ->get();

            if(count($verify_designated_point) > 0){
                return redirect()->back()->with('error', __('User already exist with default designated point person status true for this escalation level and business unit'));
            }

            $validation = [
                'name' => [
                    'required',
                    'string',
                    'max:255',
                ],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    'unique:users',
                ],
                'password' => [
                    'required',
                    'string',
                    'min:8',
                    'confirmed',
                ],
                'sbu_id' => [
                    'required',
                    'integer',
                ],
                'escalation_levels' => [
                    'required',
                    'integer',
                ],

            ];


// dd($request);

            if($request->avatar)
            {
                $validation['avatar'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
            }
            $request->validate($validation);

            $post = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'parent' => Auth::user()->getCreatedBy(),
                'sbu_id' => $request->sbu_id,
                'escalation_levels'  =>   $request->escalation_levels,
                'is_desgnated_point_person'    => $boolean_role
            ];

            if($request->avatar)
            {
                $avatarName = 'avatar-' . time() . '.' . $request->avatar->getClientOriginalExtension();
                $request->avatar->storeAs('public', $avatarName);
                $post['avatar'] = $avatarName;
            }


            $user = User::create($post);
            $role = Role::find($request->role);

            if($role)
            {
                $user->assignRole($role);
            }

            return redirect()->route('admin.users')->with('success', __('User created successfully'));
        }
        else
        {
            return view('403');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $userObj = \Auth::user();
        if($userObj->can('edit-users') || $user->id == $userObj->id)
        {
            $roles = Role::get();

            return view('admin.users.edit', compact('user', 'userObj', 'roles'));
        }
        else
        {
            return view('403');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if(!App::environment('demo'))
        {
            $userObj = \Auth::user();
            if($userObj->can('edit-users') || $user->id == $userObj->id)
            {
                $user->update(
                    $request->only(
                        [
                            'name',
                            'email',
                        ]
                    )
                );

                if($request->password)
                {
                    $user->update(['password' => Hash::make($request->password)]);
                }

                if($request->avatar)
                {
                    $request->validate(['avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

                    $avatarName = 'avatar-' . time() . '.' . $request->avatar->getClientOriginalExtension();
                    $request->avatar->storeAs('public', $avatarName);
                    $user->update(['avatar' => $avatarName]);
                }

                if($request->role && $request->user()->can('edit-users') && !$user->isme)
                {
                    $role = Role::find($request->role);
                    if($role)
                    {
                        $user->syncRoles([$role]);
                    }
                }

                return redirect()->route('admin.users')->with('success', __('User updated successfully'));
            }
        }
        else
        {
            return view('403');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if(!App::environment('demo') && !$user->isme)
        {
            $user1 = \Auth::user();
            if($user1->can('delete-users'))
            {
                $user->delete();

                return redirect()->route('admin.users')->with('success', __('User deleted successfully'));
            }
            else
            {
                return view('403');
            }
        }
        else
        {
            return redirect()->route('admin.users')->with('error', __('User accounts cannot be deleted in demo mode.'));
        }
    }

    public function roles()
    {
        return response()->json(Role::get());
    }

    public function setting()
    {
        $user = \Auth::user();
        if($user->can('manage-setting'))
        {
            $lang = $user->languages();

            return view('admin.users.setting', compact('lang'));
        }
        else
        {
            return view('403');
        }
    }

    public function save_setting(Request $request)
    {
        $user = \Auth::user();
        if($user->can('manage-setting'))
        {
            if($request->logo)
            {
                $request->validate(['logo' => 'required|image|mimes:png|max:1024']);
                $logoName = 'logo.png';
                $request->logo->storeAs('', $logoName);
            }
            $request->validate(
                [
                    'mail_driver' => 'required|string|max:50',
                    'mail_host' => 'required|string|max:50',
                    'mail_port' => 'required|string|max:50',
                    'mail_username' => 'required|string|max:50',
                    'mail_password' => 'required|string|max:50',
                    //                    'mail_encryption' => 'required|string|max:50',
                ]
            );
            $path = base_path('.env');

            if(file_exists($path))
            {
                file_put_contents(
                    $path, str_replace(
                             'MAIL_DRIVER=' . env('MAIL_DRIVER'), "MAIL_DRIVER=" . addslashes($request->mail_driver) . "", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'MAIL_HOST=' . env('MAIL_HOST'), "MAIL_HOST=" . addslashes($request->mail_host) . "", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'MAIL_PORT=' . env('MAIL_PORT'), "MAIL_PORT=" . addslashes($request->mail_port) . "", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'MAIL_USERNAME=' . ((env('MAIL_USERNAME') == NULL) ? 'null' : ("'" . env('MAIL_USERNAME')) . "'"), "MAIL_USERNAME=" . addslashes($request->mail_username) . "", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'MAIL_PASSWORD=' . ((env('MAIL_PASSWORD') == NULL) ? 'null' : ("'" . env('MAIL_PASSWORD')) . "'"), "MAIL_PASSWORD='" . addslashes($request->mail_password) . "'", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'MAIL_ENCRYPTION=' . ((env('MAIL_ENCRYPTION') == NULL) ? 'null' : (env('MAIL_ENCRYPTION'))), "MAIL_ENCRYPTION=" . addslashes($request->mail_encryption) . "", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'CUR_LANG=' . env('CUR_LANG'), 'CUR_LANG=' . $request->lang, file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'CHAT_MODULE=' . ("'" . env('CHAT_MODULE') . "'"), "CHAT_MODULE='" . addslashes(($request->chat == 'on') ? 'yes' : 'no') . "'", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'PUSHER_APP_ID=' . ("'" . env('PUSHER_APP_ID')) . "'", "PUSHER_APP_ID='" . addslashes($request->pusher_app_id) . "'", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'PUSHER_APP_KEY=' . ("'" . env('PUSHER_APP_KEY')) . "'", "PUSHER_APP_KEY='" . addslashes($request->pusher_app_key) . "'", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'PUSHER_APP_SECRET=' . ("'" . env('PUSHER_APP_SECRET')) . "'", "PUSHER_APP_SECRET='" . addslashes($request->pusher_app_secret) . "'", file_get_contents($path)
                         )
                );
                file_put_contents(
                    $path, str_replace(
                             'PUSHER_APP_CLUSTER=' . ("'" . env('PUSHER_APP_CLUSTER')) . "'", "PUSHER_APP_CLUSTER='" . addslashes($request->pusher_app_cluster) . "'", file_get_contents($path)
                         )
                );
            }

            return redirect()->route('admin.setting')->with('success', __('Setting updated successfully'));
        }
        else
        {
            return view('403');
        }
    }

}
