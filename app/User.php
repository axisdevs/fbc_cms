<?php

namespace App;

use App\Traits\UserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use UserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'parent',
        'avatar',
        'sbu_id',
        'escalation_levels',
        'is_desgnated_point_person'
    ];

    protected $appends = [
        'allPermissions',
        'profilelink',
        'avatarlink',
        'isme',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAllpermissionsAttribute()
    {
        $res            = [];
        $allPermissions = $this->getAllPermissions();
        foreach($allPermissions as $p)
        {
            $res[] = $p->name;
        }

        return $res;
    }

    public function languages()
    {

        $dir     = base_path() . '/resources/lang/';
        $glob    = glob($dir . "*", GLOB_ONLYDIR);
        $arrLang = array_map(
            function ($value) use ($dir){
                return str_replace($dir, '', $value);
            }, $glob
        );
        $arrLang = array_map(
            function ($value) use ($dir){
                return preg_replace('/[0-9]+/', '', $value);
            }, $arrLang
        );
        $arrLang = array_filter($arrLang);

        return $arrLang;
    }

    public function currantLang()
    {
        if($this->can('lang-change'))
        {
            return $this->lang;
        }
        else
        {
            return $this->parentUser()->lang;
        }
    }

    public function currantLangPath()
    {
        if($this->can('lang-change'))
        {
            $lang = $this->lang;
            $dir  = base_path() . '/resources/lang/' . $lang . "/";
            if(!is_dir($dir) && $this->roles[0]->name != 'Admin')
            {
                $lang = $this->lang;
            }
        }
        else
        {
            $lang = $this->parentUser()->lang;
        }
        $dir = base_path() . '/resources/lang/' . $lang . "/";
        if(is_dir($dir))
        {
            return $lang;
        }
        else
        {
            return 'en';
        }
    }

    public function getCreatedBy()
    {
        $roles = $this->getRoleNames();
        if($roles == '["Admin"]')
        {
            return $this->id;
        }
        else
        {
            return $this->parent;
        }
    }

    public function parentUser()
    {
        return $this->hasOne('App\User', 'id', 'parent')->first();
    }

    public function unread()
    {
        return Message::where('from', '=', $this->id)->where('is_read', '=', 0)->count();
    }

    public function businessUnit(){
        return $this->hasOne('App\Sbu','id','sbu_id');
    }

    public function escGroup(){
        return $this->hasOne('App\EscalationGroup','id','escalation_levels');
    }
}
